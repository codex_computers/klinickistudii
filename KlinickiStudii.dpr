program KlinickiStudii;

uses
  Vcl.Forms,
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  Login in '..\Share2010\Login.pas' {frmLogin},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  Main in 'Main.pas' {frmMain},
  Master in '..\Share2010\Master.pas' {frmMaster},
  RabotnoMesto in 'RabotnoMesto.pas' {frmRabotnoMesto},
  Studija in 'Studija.pas' {frmStudija},
  Naracateli in 'Naracateli.pas' {frmNaracateli},
  TabsFormTemplate in '..\Share2010\TabsFormTemplate.pas' {frmrTabsFormTemplate},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  cxConstantsMak in '..\Share2010\cxConstantsMak.pas',
  MK in '..\Share2010\MK.pas' {frmMK},
  Vraboteni in 'Vraboteni.pas' {frmVraboteni},
  Lekovi in 'Lekovi.pas' {frmLekovi},
  ThreadBarcodeEpruveti in 'ThreadBarcodeEpruveti.pas',
  ThreadBarcodeDobrovolci in 'ThreadBarcodeDobrovolci.pas',
  Logs in 'Logs.pas' {frmLogs};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := '�������� ������';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  Application.CreateForm(TdmSys, dmSys);
  Application.CreateForm(TdmRes, dmRes);
  dmKon.aplikacija:='KS';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.


