object frmStudija: TfrmStudija
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1050#1083#1080#1085#1080#1095#1082#1072' '#1057#1090#1091#1076#1080#1112#1072
  ClientHeight = 749
  ClientWidth = 1292
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1292
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 726
    Width = 1292
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1292
    Height = 251
    Align = alTop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 251
    ClientRectRight = 1292
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1057#1090#1091#1076#1080#1112#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 0
      ParentFont = False
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1292
        Height = 227
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
          DataController.DataSource = dm.dsStudija
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 61
          end
          object cxGrid1DBTableView1STATUS: TcxGridDBColumn
            DataBinding.FieldName = 'STATUS'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1042#1086' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1047#1072#1074#1088#1096#1077#1085#1072
                ImageIndex = 3
                Value = 0
              end>
            Width = 124
          end
          object cxGrid1DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Width = 81
          end
          object cxGrid1DBTableView1KOD_STUDIJA: TcxGridDBColumn
            DataBinding.FieldName = 'KOD_STUDIJA'
            Width = 127
          end
          object cxGrid1DBTableView1NARACATEL: TcxGridDBColumn
            DataBinding.FieldName = 'NARACATEL'
            Visible = False
            Width = 107
          end
          object cxGrid1DBTableView1NARACATELNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NARACATELNAZIV'
            Width = 127
          end
          object cxGrid1DBTableView1OPIS: TcxGridDBColumn
            DataBinding.FieldName = 'OPIS'
            Width = 155
          end
          object cxGrid1DBTableView1BR_RAB_MESTA: TcxGridDBColumn
            DataBinding.FieldName = 'BR_RAB_MESTA'
            Width = 123
          end
          object cxGrid1DBTableView1BR_UCESNICI: TcxGridDBColumn
            DataBinding.FieldName = 'BR_UCESNICI'
            Width = 109
          end
          object cxGrid1DBTableView1PARTOVI: TcxGridDBColumn
            DataBinding.FieldName = 'PARTOVI'
            Width = 87
          end
          object cxGrid1DBTableView1RASTOJANIE: TcxGridDBColumn
            DataBinding.FieldName = 'RASTOJANIE'
            Width = 101
          end
          object cxGrid1DBTableView1MINUTI_PLUS_MINUS: TcxGridDBColumn
            DataBinding.FieldName = 'MINUTI_PLUS_MINUS'
            Width = 172
          end
          object cxGrid1DBTableView1CHEK_IN: TcxGridDBColumn
            DataBinding.FieldName = 'CHEK_IN'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                ImageIndex = 0
                Value = 1
              end
              item
                ImageIndex = 1
                Value = 0
              end>
          end
          object cxGrid1DBTableView1CHEK_IN_RASTOJANIE: TcxGridDBColumn
            DataBinding.FieldName = 'CHEK_IN_RASTOJANIE'
            Width = 193
          end
          object cxGrid1DBTableView1BR_REZERVI: TcxGridDBColumn
            DataBinding.FieldName = 'BR_REZERVI'
            Width = 143
          end
          object cxGrid1DBTableView1REZERVI_NULTA_TOCKA: TcxGridDBColumn
            DataBinding.FieldName = 'REZERVI_NULTA_TOCKA'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                ImageIndex = 0
                Value = 1
              end
              item
                ImageIndex = 1
                Value = 0
              end>
            Width = 216
          end
          object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 150
          end
          object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 150
          end
          object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 150
          end
          object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 172
          end
          object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 212
          end
          object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 192
          end
          object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 230
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1057#1090#1091#1076#1080#1112#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object dPanel: TPanel
        Left = 0
        Top = 0
        Width = 1292
        Height = 227
        Align = alClient
        Enabled = False
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          1292
          227)
        object cxDBRadioGroup1: TcxDBRadioGroup
          Left = 27
          Top = 3
          Caption = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1089#1090#1091#1076#1080#1112#1072
          DataBinding.DataField = 'STATUS'
          DataBinding.DataSource = dm.dsStudija
          Properties.Columns = 3
          Properties.ImmediatePost = True
          Properties.Items = <
            item
              Caption = #1042#1086' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
              Value = 1
              Tag = 1
            end
            item
              Caption = #1047#1072#1074#1088#1096#1077#1085#1072
              Value = 0
            end>
          TabOrder = 0
          Height = 57
          Width = 390
        end
        object cxGroupBox1: TcxGroupBox
          Left = 27
          Top = 66
          Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1089#1090#1091#1076#1080#1112#1072
          TabOrder = 1
          Height = 145
          Width = 390
          object Label1: TLabel
            Left = 201
            Top = 31
            Width = 45
            Height = 13
            Caption = #1044#1072#1090#1091#1084' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 19
            Top = 58
            Width = 70
            Height = 13
            Caption = #1053#1072#1088#1072#1095#1072#1090#1077#1083' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 60
            Top = 31
            Width = 29
            Height = 13
            Caption = #1050#1086#1076' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 56
            Top = 82
            Width = 33
            Height = 13
            Caption = #1054#1087#1080#1089' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object NARACATEL: TcxDBLookupComboBox
            Left = 95
            Top = 55
            DataBinding.DataField = 'NARACATEL'
            DataBinding.DataSource = dm.dsStudija
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dm.dsNaracateli
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 268
          end
          object KOD_STUDIJA: TcxDBTextEdit
            Tag = 1
            Left = 95
            Top = 28
            DataBinding.DataField = 'KOD_STUDIJA'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object OPIS: TcxDBMemo
            Left = 95
            Top = 82
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 47
            Width = 268
          end
          object DATUM: TcxDBDateEdit
            Tag = 1
            Left = 252
            Top = 28
            DataBinding.DataField = 'DATUM'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 111
          end
        end
        object cxGroupBox2: TcxGroupBox
          Left = 423
          Top = 3
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1072#1088#1090#1086#1074#1080
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 2
          Height = 174
          Width = 298
          object Label5: TLabel
            Left = 20
            Top = 31
            Width = 137
            Height = 13
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label6: TLabel
            Left = 34
            Top = 58
            Width = 123
            Height = 13
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 58
            Top = 85
            Width = 99
            Height = 13
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1090#1086#1074#1080' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label8: TLabel
            Left = 38
            Top = 112
            Width = 119
            Height = 13
            Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1079#1083#1080#1082#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 269
            Top = 112
            Width = 21
            Height = 13
            Caption = #1084#1080#1085
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label16: TLabel
            Left = 41
            Top = 130
            Width = 116
            Height = 26
            Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1079#1083#1080#1082#1072' ('#1087#1083#1091#1089'/'#1084#1080#1085#1091#1089'):'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label17: TLabel
            Left = 269
            Top = 139
            Width = 21
            Height = 13
            Caption = #1084#1080#1085
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BR_RAB_MESTA: TcxDBTextEdit
            Tag = 1
            Left = 163
            Top = 28
            DataBinding.DataField = 'BR_RAB_MESTA'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object BR_UCESNICI: TcxDBTextEdit
            Tag = 1
            Left = 163
            Top = 55
            DataBinding.DataField = 'BR_UCESNICI'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object PARTOVI: TcxDBTextEdit
            Tag = 1
            Left = 163
            Top = 82
            DataBinding.DataField = 'PARTOVI'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object RASTOJANIE: TcxDBTextEdit
            Tag = 1
            Left = 163
            Top = 109
            DataBinding.DataField = 'RASTOJANIE'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object MINUTI_PLUS_MINUS: TcxDBTextEdit
            Tag = 1
            Left = 163
            Top = 136
            DataBinding.DataField = 'MINUTI_PLUS_MINUS'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
        end
        object OtkaziButton: TcxButton
          Left = 1127
          Top = 186
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 6
        end
        object ZapisiButton: TcxButton
          Left = 1046
          Top = 186
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 5
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxGroupBox4: TcxGroupBox
          Left = 727
          Top = 3
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' Chek in '#1083#1080#1089#1090#1072
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 3
          Height = 95
          Width = 281
          object Label14: TLabel
            Left = 22
            Top = 60
            Width = 119
            Height = 13
            Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1079#1083#1080#1082#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 253
            Top = 60
            Width = 21
            Height = 13
            Caption = #1084#1080#1085
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cxDBCheckBoxCHEK_IN: TcxDBCheckBox
            Left = 80
            Top = 30
            TabStop = False
            Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' Chek in '#1083#1080#1089#1090#1072
            DataBinding.DataField = 'CHEK_IN'
            DataBinding.DataSource = dm.dsStudija
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Properties.OnChange = cxDBCheckBoxCHEK_INPropertiesChange
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clNavy
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            TabOrder = 0
            Transparent = True
          end
          object CHEK_IN_RASTOJANIE: TcxDBTextEdit
            Tag = 1
            Left = 147
            Top = 57
            DataBinding.DataField = 'CHEK_IN_RASTOJANIE'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
        end
        object cxGroupBox5: TcxGroupBox
          Left = 727
          Top = 112
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1077#1079#1077#1088#1074#1085#1080' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 4
          Height = 97
          Width = 281
          object Label13: TLabel
            Left = 39
            Top = 27
            Width = 102
            Height = 26
            Alignment = taRightJustify
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1079#1077#1088#1074#1085#1080' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object BR_REZERVI: TcxDBTextEdit
            Tag = 1
            Left = 147
            Top = 32
            DataBinding.DataField = 'BR_REZERVI'
            DataBinding.DataSource = dm.dsStudija
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object cxDBCheckBoxREZERVI_NULTA_TOCKA: TcxDBCheckBox
            Left = 56
            Top = 59
            TabStop = False
            Caption = #1047#1077#1084#1072#1114#1077' '#1082#1088#1074' '#1074#1086' '#1085#1091#1083#1090#1072' '#1090#1086#1095#1082#1072
            DataBinding.DataField = 'REZERVI_NULTA_TOCKA'
            DataBinding.DataSource = dm.dsStudija
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clNavy
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            TabOrder = 1
            Transparent = True
          end
        end
      end
    end
  end
  object cxPageControl2: TcxPageControl
    Left = 0
    Top = 385
    Width = 1292
    Height = 341
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Properties.ActivePage = cxTabSheet6
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 341
    ClientRectRight = 1292
    ClientRectTop = 24
    object cxTabSheet3: TcxTabSheet
      Caption = 
        #1042#1088#1072#1073#1086#1090#1077#1085#1080', '#1073#1088#1086#1112' '#1085#1072' '#1044#1086#1073#1088#1086#1074#1086#1083#1094#1080' '#1080' '#1073#1088#1086#1112' '#1085#1072' '#1056#1077#1079#1077#1088#1074#1085#1080' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080' '#1088#1072#1073#1086 +
        #1090#1085#1086' '#1084#1077#1089#1090#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 0
      ParentFont = False
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1292
        Height = 317
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid2DBTableView1KeyDown
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsRabMestoVrabotenUcesnici
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skSum
              Column = cxGrid2DBTableView1CUSTOM1
            end
            item
              Kind = skSum
            end
            item
              Kind = skCount
            end
            item
              Kind = skSum
              Column = cxGrid2DBTableView1CUSTOM3
            end
            item
              Kind = skSum
              Column = cxGrid2DBTableView1BR_REZERVI
            end
            item
              Kind = skSum
              Column = cxGrid2DBTableView1ID
            end
            item
              Kind = skSum
              Column = cxGrid2DBTableView1BR_UCESNICI
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.OnGetFooterStyle = cxGrid2DBTableView1StylesGetFooterStyle
          object cxGrid2DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
            Width = 63
          end
          object cxGrid2DBTableView1STUDIJA: TcxGridDBColumn
            DataBinding.FieldName = 'STUDIJA'
            Visible = False
            Options.Editing = False
            Width = 144
          end
          object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid2DBTableView1VRABOTEN: TcxGridDBColumn
            DataBinding.FieldName = 'VRABOTEN'
            Visible = False
            Width = 108
          end
          object cxGrid2DBTableView1IME_PREZIME: TcxGridDBColumn
            DataBinding.FieldName = 'VRABOTEN'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'IME_PREZIME'
              end>
            Properties.ListSource = dm.dsVraboteni
            Properties.OnCloseUp = cxGrid2DBTableView1IME_PREZIMEPropertiesCloseUp
            Styles.Content = dm.cxStyle100
            Width = 208
          end
          object cxGrid2DBTableView1BR_UCESNICI: TcxGridDBColumn
            DataBinding.FieldName = 'BR_UCESNICI'
            Styles.Content = dm.cxStyle100
            Width = 110
          end
          object cxGrid2DBTableView1BR_REZERVI: TcxGridDBColumn
            DataBinding.FieldName = 'BR_REZERVI'
            Styles.Content = dm.cxStyle100
            Width = 156
          end
          object cxGrid2DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid2DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid2DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 163
          end
          object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 211
          end
          object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 212
          end
          object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 236
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = #1051#1077#1082#1086#1074#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1292
        Height = 276
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid3DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid3DBTableView1KeyDown
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsStudijaLekovi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.GroupByBox = False
          object cxGrid3DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
            Width = 52
          end
          object cxGrid3DBTableView1STUDIJA_ID: TcxGridDBColumn
            DataBinding.FieldName = 'STUDIJA_ID'
            Visible = False
            Options.Editing = False
            Width = 97
          end
          object cxGrid3DBTableView1LEK_ID: TcxGridDBColumn
            DataBinding.FieldName = 'LEK_ID'
            Visible = False
            Options.Editing = False
            Width = 82
          end
          object cxGrid3DBTableView1LEK_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'LEK_NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid3DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082' ('#1084#1083')'
            DataBinding.FieldName = 'PRIMEROK_KOLICINA'
            Styles.Content = dm.cxStyle100
            Width = 150
          end
          object cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid3DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 167
          end
          object cxGrid3DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 216
          end
          object cxGrid3DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 203
          end
          object cxGrid3DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 150
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 276
        Width = 1292
        Height = 41
        Align = alBottom
        TabOrder = 1
        object cxButton3: TcxButton
          Left = 30
          Top = 10
          Width = 99
          Height = 25
          Caption = #1044#1086#1076#1072#1076#1080' '#1083#1077#1082
          Colors.Pressed = clGradientActiveCaption
          OptionsImage.ImageIndex = 90
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 0
          OnClick = Button1Click
        end
        object cxButton4: TcxButton
          Left = 135
          Top = 10
          Width = 106
          Height = 25
          Caption = #1048#1079#1074#1072#1076#1080' '#1083#1077#1082
          Colors.Pressed = clGradientActiveCaption
          OptionsImage.ImageIndex = 91
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 1
          OnClick = Button2Click
        end
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = #1055#1072#1088#1090#1086#1074#1080' '#1080' '#1074#1088#1077#1084#1077#1085#1089#1082#1080' '#1090#1086#1095#1082#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      object cxGrid4: TcxGrid
        Left = 0
        Top = 0
        Width = 575
        Height = 317
        Align = alLeft
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid4DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid4DBTableView1KeyDown
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsStudijaPart
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.GroupByBox = False
          object cxGrid4DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
            Width = 68
          end
          object cxGrid4DBTableView1PART: TcxGridDBColumn
            DataBinding.FieldName = 'PART'
            Options.Editing = False
            Styles.OnGetContentStyle = cxGrid4DBTableView1PARTStylesGetContentStyle
            Width = 150
          end
          object cxGrid4DBTableView1STUDIJA: TcxGridDBColumn
            DataBinding.FieldName = 'STUDIJA'
            Visible = False
            Options.Editing = False
            Width = 95
          end
          object cxGrid4DBTableView1POCETNO_VREME: TcxGridDBColumn
            Caption = #1042#1088#1077#1084#1077
            DataBinding.FieldName = 'POCETNO_VREME'
            PropertiesClassName = 'TcxTimeEditProperties'
            Properties.AutoCorrectHours = False
            Properties.TimeFormat = tfHourMin
            Properties.UseCtrlIncrement = True
            Styles.Content = dmRes.Summer
            Styles.OnGetContentStyle = cxGrid4DBTableView1POCETNO_VREMEStylesGetContentStyle
            Width = 129
          end
          object cxGrid4DBTableView1POCETEN_DATUM: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'POCETEN_DATUM'
            Styles.OnGetContentStyle = cxGrid4DBTableView1POCETEN_DATUMStylesGetContentStyle
            Width = 128
          end
          object cxGrid4DBTableView1BROJ_TOCKI: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TOCKI'
            Styles.OnGetContentStyle = cxGrid4DBTableView1BROJ_TOCKIStylesGetContentStyle
            Width = 153
          end
          object cxGrid4DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 161
          end
          object cxGrid4DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 206
          end
          object cxGrid4DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 234
          end
          object cxGrid4DBTableView1TIP: TcxGridDBColumn
            DataBinding.FieldName = 'TIP'
            Visible = False
          end
        end
        object cxGrid4Level1: TcxGridLevel
          GridView = cxGrid4DBTableView1
        end
      end
      object cxGrid5: TcxGrid
        Left = 705
        Top = 0
        Width = 462
        Height = 317
        Align = alRight
        TabOrder = 1
        object cxGrid5DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid5DBTableView1KeyDown
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsPartTocki
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object cxGrid5DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 51
          end
          object cxGrid5DBTableView1PART_ID: TcxGridDBColumn
            DataBinding.FieldName = 'PART_ID'
            Visible = False
            Width = 82
          end
          object cxGrid5DBTableView1VREMENSKA_TOCKA: TcxGridDBColumn
            DataBinding.FieldName = 'VREMENSKA_TOCKA'
            Styles.Content = dm.cxStyle100
            Width = 205
          end
          object cxGrid5DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'PRIMEROK_KOLICINA'
            Styles.Content = dm.cxStyle100
            Width = 130
          end
          object cxGrid5DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 150
          end
          object cxGrid5DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 150
          end
          object cxGrid5DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 150
          end
          object cxGrid5DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 185
          end
          object cxGrid5DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 215
          end
          object cxGrid5DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 198
          end
          object cxGrid5DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 237
          end
          object cxGrid5DBTableView1BR_EPRUVETI: TcxGridDBColumn
            Caption = #1041#1088'.'#1077#1087#1088#1091#1074#1077#1090#1080
            DataBinding.FieldName = 'BR_EPRUVETI'
            Width = 85
          end
        end
        object cxGrid5Level1: TcxGridLevel
          GridView = cxGrid5DBTableView1
        end
      end
      object Panel2: TPanel
        Left = 1167
        Top = 0
        Width = 125
        Height = 317
        Align = alRight
        TabOrder = 2
        object cxButton2: TcxButton
          Left = 1
          Top = 1
          Width = 123
          Height = 315
          Align = alClient
          Action = aGenerirajPrimeroci
          OptionsImage.ImageIndex = 92
          OptionsImage.Images = dmRes.cxLargeImages
          OptionsImage.Layout = blGlyphTop
          TabOrder = 0
          WordWrap = True
        end
      end
      object Panel3: TPanel
        Left = 575
        Top = 0
        Width = 125
        Height = 317
        Align = alLeft
        Alignment = taRightJustify
        TabOrder = 3
        object cxButton1: TcxButton
          Left = 1
          Top = 1
          Width = 123
          Height = 315
          Align = alClient
          Action = aGenerirajPartTocki
          OptionsImage.Images = dmRes.cxLargeImages
          OptionsImage.Layout = blGlyphTop
          TabOrder = 0
          WordWrap = True
        end
      end
    end
    object cxTabSheet6: TcxTabSheet
      Caption = 'Chek in '#1083#1080#1089#1090#1072
      ImageIndex = 4
      object cxGrid7: TcxGrid
        Left = 0
        Top = 0
        Width = 1292
        Height = 317
        Align = alClient
        TabOrder = 0
        object cxGrid7DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsChekIn
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          object cxGrid7DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 90
          end
          object cxGrid7DBTableView1STUDIJA: TcxGridDBColumn
            DataBinding.FieldName = 'STUDIJA'
            Visible = False
            Width = 93
          end
          object cxGrid7DBTableView1RAB_MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'RAB_MESTO'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1RAB_MESTO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'RAB_MESTO_NAZIV'
            Width = 150
          end
          object cxGrid7DBTableView1PART: TcxGridDBColumn
            DataBinding.FieldName = 'PART'
            Visible = False
            Width = 85
          end
          object cxGrid7DBTableView1PART_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PART_NAZIV'
            Visible = False
            Width = 132
          end
          object cxGrid7DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Width = 88
          end
          object cxGrid7DBTableView1VREME: TcxGridDBColumn
            DataBinding.FieldName = 'VREME'
            Width = 89
          end
          object cxGrid7DBTableView1DOBROVOLEC: TcxGridDBColumn
            DataBinding.FieldName = 'DOBROVOLEC'
            Width = 82
          end
          object cxGrid7DBTableView1AKTIVEN_DOBROVOLEC: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN_DOBROVOLEC'
            Visible = False
            Width = 128
          end
          object cxGrid7DBTableView1TOCKA_BR: TcxGridDBColumn
            DataBinding.FieldName = 'TOCKA_BR'
            Visible = False
            Width = 143
          end
          object cxGrid7DBTableView1VREMENSKA_TOCKA: TcxGridDBColumn
            DataBinding.FieldName = 'VREMENSKA_TOCKA'
            Visible = False
            Width = 100
          end
          object cxGrid7DBTableView1REDEN_BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'REDEN_BROJ'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1BROJ_EPRUVETA: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_EPRUVETA'
            Width = 106
          end
          object cxGrid7DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'PRIMEROK_KOLICINA'
            Width = 141
          end
          object cxGrid7DBTableView1VREME_REALIZACIJA: TcxGridDBColumn
            DataBinding.FieldName = 'VREME_REALIZACIJA'
            Width = 130
          end
          object cxGrid7DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 150
          end
          object cxGrid7DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 150
          end
        end
        object cxGrid7Level1: TcxGridLevel
          GridView = cxGrid7DBTableView1
        end
      end
    end
    object cxTabSheet7: TcxTabSheet
      Caption = #1055#1088#1080#1084#1077#1088#1086#1094#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 4
      ParentFont = False
      object cxGrid6: TcxGrid
        Left = 0
        Top = 0
        Width = 1292
        Height = 317
        Align = alClient
        TabOrder = 0
        object cxGrid6DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid6DBTableView1KeyDown
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsPrimeroci
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          Styles.OnGetContentStyle = cxGrid6DBTableView1StylesGetContentStyle
          object cxGrid6DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
          end
          object cxGrid6DBTableView1STUDIJA: TcxGridDBColumn
            DataBinding.FieldName = 'STUDIJA'
            Visible = False
            Options.Editing = False
            Width = 97
          end
          object cxGrid6DBTableView1RAB_MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'RAB_MESTO'
            Visible = False
            Options.Editing = False
            Width = 127
          end
          object cxGrid6DBTableView1RAB_MESTO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'RAB_MESTO_NAZIV'
            Options.Editing = False
            Width = 103
          end
          object cxGrid6DBTableView1PART: TcxGridDBColumn
            Caption = #1055#1072#1088#1090' '#1090#1086#1095#1082#1080' - '#1096#1080#1092#1088#1072
            DataBinding.FieldName = 'PART'
            Visible = False
            Options.Editing = False
            Width = 111
          end
          object cxGrid6DBTableView1PART_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PART_NAZIV'
            Options.Editing = False
            Width = 151
          end
          object cxGrid6DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Options.Editing = False
            Width = 72
          end
          object cxGrid6DBTableView1VREME: TcxGridDBColumn
            DataBinding.FieldName = 'VREME'
            PropertiesClassName = 'TcxTimeEditProperties'
            Properties.AutoCorrectHours = False
            Properties.ReadOnly = False
            Properties.TimeFormat = tfHourMin
            Properties.UseCtrlIncrement = True
            Options.Editing = False
            Width = 77
          end
          object cxGrid6DBTableView1AKTIVEN_DOBROVOLEC: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN_DOBROVOLEC'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Properties.OnEditValueChanged = cxGrid6DBTableView1AKTIVEN_DOBROVOLECPropertiesEditValueChanged
            Styles.Content = dm.cxStyle100
            Width = 130
          end
          object cxGrid6DBTableView1DOBROVOLEC: TcxGridDBColumn
            DataBinding.FieldName = 'DOBROVOLEC'
            Options.Editing = False
            Width = 103
          end
          object cxGrid6DBTableView1VREMENSKA_TOCKA: TcxGridDBColumn
            DataBinding.FieldName = 'VREMENSKA_TOCKA'
            Options.Editing = False
            Width = 94
          end
          object cxGrid6DBTableView1BROJ_EPRUVETA: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_EPRUVETA'
            Options.Editing = False
            Width = 98
          end
          object cxGrid6DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'PRIMEROK_KOLICINA'
            Width = 157
          end
          object cxGrid6DBTableView1VREME_REALIZACIJA: TcxGridDBColumn
            DataBinding.FieldName = 'VREME_REALIZACIJA'
            Styles.Content = dm.cxStyle100
            Width = 130
          end
          object cxGrid6DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid6DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid6DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid6DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 167
          end
          object cxGrid6DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 221
          end
          object cxGrid6DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 211
          end
          object cxGrid6DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 208
          end
        end
        object cxGrid6Level1: TcxGridLevel
          GridView = cxGrid6DBTableView1
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 377
    Width = 1292
    Height = 8
    AlignSplitter = salTop
    Control = cxPageControl1
  end
  object PanelPecatiLista: TPanel
    Left = 472
    Top = 132
    Width = 342
    Height = 206
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    DesignSize = (
      342
      206)
    object cxGroupBox3: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1072#1088#1090' '#1080' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      TabOrder = 0
      DesignSize = (
        310
        174)
      Height = 174
      Width = 310
      object Label9: TLabel
        Left = 83
        Top = 38
        Width = 35
        Height = 13
        Caption = #1055#1072#1088#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 25
        Top = 60
        Width = 93
        Height = 13
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 11
        Top = 87
        Width = 107
        Height = 13
        Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1090#1086#1095#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbPart: TcxLookupComboBox
        Left = 124
        Top = 30
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'PART'
          end>
        Properties.ListSource = dm.dsIzvestajPart
        Properties.OnChange = cbPartPropertiesChange
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 145
      end
      object cbRabMesto: TcxLookupComboBox
        Left = 124
        Top = 57
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsIzvestajRabMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 145
      end
      object cxButton5: TcxButton
        Left = 64
        Top = 125
        Width = 125
        Height = 25
        Action = aPecatenjeLista
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 3
      end
      object cxButton6: TcxButton
        Left = 195
        Top = 125
        Width = 75
        Height = 25
        Action = aOtkazi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 4
      end
      object cbVremenskaTocka: TcxLookupComboBox
        Left = 124
        Top = 84
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'VREMENSKA_TOCKA'
          end>
        Properties.ListSource = dm.dslIzvestajVremenskiTocki
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 145
      end
    end
  end
  object PanelPecatiBarkodDobrovolci: TPanel
    Left = 624
    Top = 132
    Width = 256
    Height = 168
    TabOrder = 10
    Visible = False
    object cxGroupBoxBarkode: TcxGroupBox
      Left = 15
      Top = 15
      Caption = #1041#1072#1088#1082#1086#1076' '#1044#1054#1041#1056#1054#1042#1054#1051#1062#1048
      TabOrder = 0
      DesignSize = (
        226
        138)
      Height = 138
      Width = 226
      object Label18: TLabel
        Left = 33
        Top = 33
        Width = 52
        Height = 13
        Caption = #1041#1088#1086#1112' '#1054#1044' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 33
        Top = 60
        Width = 52
        Height = 13
        Caption = #1041#1088#1086#1112' '#1044#1054' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BROJ_OD: TcxTextEdit
        Left = 91
        Top = 30
        Hint = #1041#1088#1086#1112' '#1054#1044' '#1085#1072' '#1044#1054#1041#1056#1054#1042#1054#1051#1045#1062
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 118
      end
      object BROJ_DO: TcxTextEdit
        Left = 91
        Top = 57
        Hint = #1041#1088#1086#1112' '#1044#1054' '#1085#1072' '#1044#1054#1041#1056#1054#1042#1054#1051#1045#1062
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 118
      end
      object btnPecatiBD: TcxButton
        Left = 57
        Top = 98
        Width = 75
        Height = 25
        Action = aPecatiBD
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 2
        OnKeyDown = EnterKakoTab
      end
      object btnOdtaziBD: TcxButton
        Left = 138
        Top = 98
        Width = 75
        Height = 25
        Action = aOdkaziBD
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 3
        OnKeyDown = EnterKakoTab
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 72
    Top = 576
  end
  object PopupMenu1: TPopupMenu
    Left = 288
    Top = 520
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 240
    Top = 536
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 375
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1164
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' - '#1050#1083#1080#1085#1080#1095#1082#1080' '#1089#1090#1091#1076#1080#1080
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aFormularPodgotovkaEpruvetaPufer
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aZacuvajExcelPrimeroci
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiTabelaPrimeroci
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 74
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = aListaPrimerociSite
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aListaPrimerociSitePoPartRabotnoMesto
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aListaPrimerociSitePoPartRabotnoMesto
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aFormularBrzoSmrznuvanje
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aFormularVremenskaListaCentrifugiranje
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aFormularVremenskaListaSkladiranje
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aFormularVremenskaListaCentrifugiranje
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aFormularBrzoSmrznuvanje
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton7: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton8: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton9: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1060#1086#1088#1084#1091#1083#1072#1088#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end>
    end
    object dxBarButton10: TdxBarButton
      Action = aFormularBrzoSmrznuvanje
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = aFormularPodgotovkaEpruvetaPufer
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = aFormularVremenskaListaCentrifugiranje
      Category = 0
    end
    object dxBarButton13: TdxBarButton
      Action = aFormularVremenskaListaSkladiranje
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aPecatiSiteFormulari
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aPChekInLista
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1041#1072#1088#1082#1086#1076' '#1083#1072#1073#1077#1083#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 97
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarButton17'
        end>
    end
    object dxBarButton14: TdxBarButton
      Action = aBarkodDobrovolci
      Category = 0
    end
    object dxBarButton15: TdxBarButton
      Action = aBarkodEpruveti
      Category = 0
    end
    object dxBarButton16: TdxBarButton
      Action = aBarkodDobrovolci
      Category = 0
    end
    object dxBarSubItem5: TdxBarSubItem
      Action = aBarkodEpruveti
      Category = 0
      ItemLinks = <>
    end
    object dxBarButton17: TdxBarButton
      Action = aBarkodEpruveti
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aOsveziListaPrimeroci
      Category = 0
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aPromeniStatusStudija
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aPVremeRealizacija
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 992
    Top = 72
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel - '#1050#1083#1080#1085#1080#1095#1082#1080' '#1089#1090#1091#1076#1080#1080
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' - '#1050#1083#1080#1085#1080#1095#1082#1080' '#1089#1090#1091#1076#1080#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aGenerirajPrimeroci: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 70
      OnExecute = aGenerirajPrimerociExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aGenerirajRabMestaUcenici: TAction
      Caption = 'aGenerirajRabMestaUcenici'
    end
    object aGenerirajPartTocki: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1074#1088#1077#1084#1077#1085#1089#1082#1080' '#1090#1086#1095#1082#1080' '#1079#1072' '#1087#1072#1088#1090
      ImageIndex = 90
      OnExecute = aGenerirajPartTockiExecute
    end
    object abrisiPrimeroci: TAction
      Caption = #1041#1088#1080#1096#1080' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 71
      OnExecute = abrisiPrimerociExecute
    end
    object aListaPrimerociSite: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080' - '#1089#1080#1090#1077
      ImageIndex = 74
      OnExecute = aListaPrimerociSiteExecute
    end
    object aDizajnReport: TAction
      Caption = 'aDizajnReport'
      ShortCut = 24697
      OnExecute = aDizajnReportExecute
    end
    object aDListaPrimeroci: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      OnExecute = aDListaPrimerociExecute
    end
    object aFormularPodgotovkaEpruvetaPufer: TAction
      Caption = #1060#1086#1088#1084#1091#1083#1072#1088' '#1079#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' '#1085#1072' '#1077#1087#1088#1091#1074#1077#1090#1080' '#1089#1086' '#1087#1091#1092#1077#1088
      ImageIndex = 19
      OnExecute = aFormularPodgotovkaEpruvetaPuferExecute
    end
    object aDFormularPodgotovkaEpruvetaPufer: TAction
      Caption = #1060#1086#1088#1084#1091#1083#1072#1088' '#1079#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' '#1085#1072' '#1077#1087#1088#1091#1074#1077#1090#1080' '#1089#1086' '#1087#1091#1092#1077#1088
      OnExecute = aDFormularPodgotovkaEpruvetaPuferExecute
    end
    object aZacuvajExcelPrimeroci: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel - '#1055#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcelPrimerociExecute
    end
    object aPecatiTabelaPrimeroci: TAction
      Caption = #1055#1077#1095#1072#1090#1080' - '#1055#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaPrimerociExecute
    end
    object aListaPrimerociSitePoPartRabotnoMesto: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 89
      OnExecute = aListaPrimerociSitePoPartRabotnoMestoExecute
    end
    object aOtkaziPecatenjeLista: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 71
    end
    object aPecatenjeLista: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      ImageIndex = 19
      OnExecute = aPecatenjeListaExecute
    end
    object aFormularBrzoSmrznuvanje: TAction
      Caption = #1060#1086#1088#1084#1091#1083#1072#1088' '#1079#1072' '#1073#1088#1079#1086' '#1089#1084#1088#1079#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 19
      OnExecute = aFormularBrzoSmrznuvanjeExecute
    end
    object aDFormularBrzoSmrznuvanje: TAction
      Caption = #1060#1086#1088#1084#1091#1083#1072#1088' '#1079#1072' '#1073#1088#1079#1086' '#1089#1084#1088#1079#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      OnExecute = aDFormularBrzoSmrznuvanjeExecute
    end
    object aFormularVremenskaListaCentrifugiranje: TAction
      Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1083#1080#1089#1090#1072' '#1079#1072' '#1094#1077#1085#1090#1088#1080#1092#1091#1075#1080#1088#1072#1114#1077
      ImageIndex = 19
      OnExecute = aFormularVremenskaListaCentrifugiranjeExecute
    end
    object aDFormularVremenskaListaCentrifugiranje: TAction
      Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1083#1080#1089#1090#1072' '#1079#1072' '#1094#1077#1085#1090#1088#1080#1092#1091#1075#1080#1088#1072#1114#1077
      OnExecute = aDFormularVremenskaListaCentrifugiranjeExecute
    end
    object aFormularVremenskaListaSkladiranje: TAction
      Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1083#1080#1089#1090#1072' '#1079#1072' '#1089#1082#1083#1072#1076#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080' '#1074#1086' '#1092#1088#1080#1078#1080#1076#1077#1088
      ImageIndex = 19
      OnExecute = aFormularVremenskaListaSkladiranjeExecute
    end
    object aDFormularVremenskaListaSkladiranje: TAction
      Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1083#1080#1089#1090#1072' '#1079#1072' '#1089#1082#1083#1072#1076#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080' '#1074#1086' '#1092#1088#1080#1078#1080#1076#1077#1088
      OnExecute = aDFormularVremenskaListaSkladiranjeExecute
    end
    object aPecatiSiteFormulari: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1075#1080' '#1089#1080#1090#1077' '#1092#1086#1088#1084#1091#1083#1072#1088#1080
      ImageIndex = 30
      OnExecute = aPecatiSiteFormulariExecute
    end
    object aPChekInLista: TAction
      Caption = 'Chek in '#1083#1080#1089#1090#1072
      ImageIndex = 79
      OnExecute = aPChekInListaExecute
    end
    object aDChekInLista: TAction
      Caption = 'Chek in '#1083#1080#1089#1090#1072
      OnExecute = aDChekInListaExecute
    end
    object aBarkodEpruveti: TAction
      Caption = #1041#1072#1088#1082#1086#1076' '#1083#1072#1073#1077#1083#1080' - '#1045#1087#1088#1091#1074#1077#1090#1080
      ImageIndex = 98
      OnExecute = aBarkodEpruvetiExecute
    end
    object aBarkodDobrovolci: TAction
      Caption = #1041#1072#1088#1082#1086#1076' '#1083#1072#1073#1077#1083#1080' - '#1044#1086#1073#1088#1086#1074#1086#1083#1094#1080
      ImageIndex = 98
      OnExecute = aBarkodDobrovolciExecute
    end
    object aDBarkodDobrovolci: TAction
      Caption = #1041#1072#1088#1082#1086#1076' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080
      OnExecute = aDBarkodDobrovolciExecute
    end
    object aDBarkodEpruveti: TAction
      Caption = #1041#1072#1088#1082#1086#1076' '#1077#1087#1088#1091#1074#1077#1090#1080
      OnExecute = aDBarkodEpruvetiExecute
    end
    object aOsveziListaPrimeroci: TAction
      Caption = #1054#1089#1074#1077#1078#1080' '#1112#1072' '#1083#1080#1089#1090#1072#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1094#1080
      ImageIndex = 18
      OnExecute = aOsveziListaPrimerociExecute
    end
    object aPromeniStatusStudija: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1089#1090#1072#1090#1091#1089
      ImageIndex = 85
      OnExecute = aPromeniStatusStudijaExecute
    end
    object aOdkaziBD: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 37
      OnExecute = aOdkaziBDExecute
    end
    object aPecatiBD: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiBDExecute
    end
    object aPVremeRealizacija: TAction
      Caption = 'Blood sampling time record'
      ImageIndex = 92
      OnExecute = aPVremeRealizacijaExecute
    end
    object aDVremeRealizacija: TAction
      Caption = 'Blood sampling time record'
      OnExecute = aDVremeRealizacijaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Component = cxGrid6
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 496
    Top = 560
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 32
    Top = 502
    object N6: TMenuItem
      Action = aDChekInLista
    end
    object aDMesecenIzvestaj1: TMenuItem
      Action = aDListaPrimeroci
    end
    object N2: TMenuItem
      Action = aDFormularPodgotovkaEpruvetaPufer
    end
    object N3: TMenuItem
      Action = aDFormularBrzoSmrznuvanje
    end
    object N4: TMenuItem
      Action = aDFormularVremenskaListaCentrifugiranje
    end
    object N5: TMenuItem
      Action = aDFormularVremenskaListaSkladiranje
    end
    object N7: TMenuItem
      Action = aDBarkodDobrovolci
    end
    object N8: TMenuItem
      Action = aDBarkodEpruveti
    end
    object Bloodsamplingtimerecord1: TMenuItem
      Action = aDVremeRealizacija
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 168
    Top = 488
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 240
    Top = 592
  end
  object cxGridPopupMenu4: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 224
    Top = 520
  end
  object cxGridPopupMenu5: TcxGridPopupMenu
    Grid = cxGrid5
    PopupMenus = <>
    Left = 984
    Top = 504
  end
  object cxGridPopupMenu6: TcxGridPopupMenu
    Grid = cxGrid6
    PopupMenus = <>
    Left = 152
    Top = 600
  end
  object cxGridPopupMenu7: TcxGridPopupMenu
    Grid = cxGrid7
    PopupMenus = <>
    Left = 88
    Top = 512
  end
  object cxGridPopupMenu8: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 264
    Top = 584
  end
end
