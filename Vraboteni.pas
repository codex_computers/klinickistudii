unit Vraboteni;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData,
  cxContainer, Vcl.Menus, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore, dxPScxCommon,
  System.Actions, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxCheckBox, cxGroupBox, cxImageComboBox;

type
  TfrmVraboteni = class(TfrmMaster)
    Label2: TLabel;
    IME_PREZIME: TcxDBTextEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    KORISNICKO_IME: TcxDBTextEdit;
    Label4: TLabel;
    LOZINKA: TcxDBTextEdit;
    cxDBCheckBox1: TcxDBCheckBox;
    cxGrid1DBTableView1LOZINKA: TcxGridDBColumn;
    cxGrid1DBTableView1KORISNICKO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1PRAVA: TcxGridDBColumn;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVraboteni: TfrmVraboteni;

implementation

{$R *.dfm}

uses dmUnit, dmResources;

procedure TfrmVraboteni.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
    if((Sender = KORISNICKO_IME) or (Sender = LOZINKA))then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

procedure TfrmVraboteni.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if((Sender = KORISNICKO_IME) or (Sender = LOZINKA))then
            ActivateKeyboardLayout($042F042F, KLF_REORDER);
       end;
end;

end.
