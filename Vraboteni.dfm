inherited frmVraboteni: TfrmVraboteni
  Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080
  ClientHeight = 618
  ClientWidth = 754
  ExplicitWidth = 762
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 754
    Height = 258
    ExplicitWidth = 754
    ExplicitHeight = 258
    inherited cxGrid1: TcxGrid
      Width = 750
      Height = 254
      ExplicitWidth = 750
      ExplicitHeight = 254
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsVraboteni
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 67
        end
        object cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'IME_PREZIME'
          Width = 245
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1KORISNICKO_IME: TcxGridDBColumn
          DataBinding.FieldName = 'KORISNICKO_IME'
          Width = 95
        end
        object cxGrid1DBTableView1LOZINKA: TcxGridDBColumn
          DataBinding.FieldName = 'LOZINKA'
          Width = 90
        end
        object cxGrid1DBTableView1PRAVA: TcxGridDBColumn
          DataBinding.FieldName = 'PRAVA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1048#1084#1072
              ImageIndex = 2
              Value = 1
            end
            item
              Description = #1053#1077#1084#1072
              ImageIndex = 3
              Value = 0
            end>
          Width = 228
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 384
    Width = 754
    Height = 211
    ExplicitTop = 384
    ExplicitWidth = 754
    ExplicitHeight = 211
    inherited Label1: TLabel
      Left = 74
      Top = 24
      ExplicitLeft = 74
      ExplicitTop = 24
    end
    object Label2: TLabel [1]
      Left = 31
      Top = 48
      Width = 89
      Height = 13
      Caption = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 126
      Top = 21
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsVraboteni
      ExplicitLeft = 126
      ExplicitTop = 21
    end
    inherited OtkaziButton: TcxButton
      Left = 663
      Top = 171
      TabOrder = 4
      ExplicitLeft = 663
      ExplicitTop = 171
    end
    inherited ZapisiButton: TcxButton
      Left = 582
      Top = 171
      TabOrder = 3
      ExplicitLeft = 582
      ExplicitTop = 171
    end
    object IME_PREZIME: TcxDBTextEdit
      Tag = 1
      Left = 126
      Top = 45
      BeepOnEnter = False
      DataBinding.DataField = 'IME_PREZIME'
      DataBinding.DataSource = dm.dsVraboteni
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 427
    end
    object cxGroupBox1: TcxGroupBox
      Left = 126
      Top = 72
      Caption = #1052#1086#1073#1080#1083#1085#1072' '#1072#1087#1083#1080#1082#1072#1094#1080#1112#1072
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 2
      Height = 102
      Width = 419
      object Label3: TLabel
        Left = 23
        Top = 32
        Width = 97
        Height = 13
        Caption = #1050#1086#1088#1080#1089#1085#1080#1095#1082#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 234
        Top = 32
        Width = 54
        Height = 13
        Caption = #1051#1086#1079#1080#1085#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object KORISNICKO_IME: TcxDBTextEdit
        Left = 126
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'KORISNICKO_IME'
        DataBinding.DataSource = dm.dsVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.Shadow = False
        Style.IsFontAssigned = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 102
      end
      object LOZINKA: TcxDBTextEdit
        Left = 294
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'LOZINKA'
        DataBinding.DataSource = dm.dsVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 99
      end
      object cxDBCheckBox1: TcxDBCheckBox
        Left = 16
        Top = 59
        Caption = #1048#1084#1072' '#1087#1088#1072#1074#1086' '#1079#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1084#1086#1073#1080#1083#1085#1072' '#1072#1087#1083#1080#1082#1072#1094#1080#1112#1072
        DataBinding.DataField = 'PRAVA'
        DataBinding.DataSource = dm.dsVraboteni
        ParentFont = False
        Properties.Alignment = taRightJustify
        Properties.ImmediatePost = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 313
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 754
    ExplicitWidth = 754
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 595
    Width = 754
    ExplicitTop = 595
    ExplicitWidth = 754
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 42172.374531643520000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
