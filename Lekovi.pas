unit Lekovi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData,
  cxContainer, Vcl.Menus, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore, dxPScxCommon,
  System.Actions, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls;

type
  TfrmLekovi = class(TfrmMaster)
    Label2: TLabel;
    NAZIV: TcxDBTextEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure generirajLekoviStudija();
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLekovi: TfrmLekovi;

implementation

{$R *.dfm}

uses dmUnit;

procedure TfrmLekovi.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
    inherited;
    if(Sender = NAZIV)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

procedure TfrmLekovi.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
      if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if(Sender = NAZIV)then
            ActivateKeyboardLayout($042F042F, KLF_REORDER);
       end;
end;

procedure TfrmLekovi.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
   generirajLekoviStudija()
end;

procedure TfrmLekovi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
    if(Ord(Key) = VK_RETURN) then
    generirajLekoviStudija()
end;



procedure TfrmLekovi.generirajLekoviStudija() ;
begin
    if(not inserting) then
      begin
         ModalResult := mrOk;
         dm.insert6(dm.PROC_KS_STUDIJA_LEKOVI,'KS_ID', 'LEK_ID', 'TAG', null,null, null,dm.tblStudijaID.Value, dm.tblLekoviID.Value,1, null,null, null);
      end;

end;

end.
