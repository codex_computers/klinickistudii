unit dmUnit;

interface

uses
  System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet, cxStyles, cxClasses, Controls,
  FIBQuery, pFIBQuery, pFIBStoredProc, FIBDatabase, pFIBDatabase, Variants,
  cxCustomPivotGrid, cxTL, cxGridCardView, cxGridBandedTableView,
  cxGridTableView;

type
  Tdm = class(TDataModule)
    tblRabotnoMesto: TpFIBDataSet;
    dsRabotnoMesto: TDataSource;
    tblRabotnoMestoID: TFIBIntegerField;
    tblRabotnoMestoNAZIV: TFIBStringField;
    tblRabotnoMestoCUSTOM1: TFIBStringField;
    tblRabotnoMestoCUSTOM2: TFIBStringField;
    tblRabotnoMestoCUSTOM3: TFIBStringField;
    tblRabotnoMestoTS_INS: TFIBDateTimeField;
    tblRabotnoMestoTS_UPD: TFIBDateTimeField;
    tblRabotnoMestoUSR_INS: TFIBStringField;
    tblRabotnoMestoUSR_UPD: TFIBStringField;
    tblRabotnoMestoSTATUS: TFIBSmallIntField;
    tblNaracateli: TpFIBDataSet;
    dsNaracateli: TDataSource;
    tblNaracateliID: TFIBIntegerField;
    tblNaracateliNAZIV: TFIBStringField;
    tblNaracateliCUSTOM1: TFIBStringField;
    tblNaracateliCUSTOM2: TFIBStringField;
    tblNaracateliCUSTOM3: TFIBStringField;
    tblNaracateliTS_INS: TFIBDateTimeField;
    tblNaracateliTS_UPD: TFIBDateTimeField;
    tblNaracateliUSR_INS: TFIBStringField;
    tblNaracateliUSR_UPD: TFIBStringField;
    tblStudija: TpFIBDataSet;
    dsStudija: TDataSource;
    tblStudijaID: TFIBIntegerField;
    tblStudijaSTATUS: TFIBSmallIntField;
    tblStudijaKOD_STUDIJA: TFIBStringField;
    tblStudijaDATUM: TFIBDateField;
    tblStudijaNARACATEL: TFIBIntegerField;
    tblStudijaNARACATELNAZIV: TFIBStringField;
    tblStudijaOPIS: TFIBStringField;
    tblStudijaBR_RAB_MESTA: TFIBIntegerField;
    tblStudijaBR_UCESNICI: TFIBIntegerField;
    tblStudijaPARTOVI: TFIBIntegerField;
    tblStudijaCUSTOM1: TFIBStringField;
    tblStudijaCUSTOM2: TFIBStringField;
    tblStudijaCUSTOM3: TFIBStringField;
    tblStudijaTS_INS: TFIBDateTimeField;
    tblStudijaTS_UPD: TFIBDateTimeField;
    tblStudijaUSR_INS: TFIBStringField;
    tblStudijaUSR_UPD: TFIBStringField;
    tblVraboteni: TpFIBDataSet;
    dsVraboteni: TDataSource;
    tblVraboteniID: TFIBIntegerField;
    tblVraboteniCUSTOM1: TFIBStringField;
    tblVraboteniCUSTOM2: TFIBStringField;
    tblVraboteniCUSTOM3: TFIBStringField;
    tblVraboteniTS_INS: TFIBDateTimeField;
    tblVraboteniTS_UPD: TFIBDateTimeField;
    tblVraboteniUSR_INS: TFIBStringField;
    tblVraboteniUSR_UPD: TFIBStringField;
    tblVraboteniIME_PREZIME: TFIBStringField;
    PROC_KS_RABMESTA_VRAB_UCES: TpFIBStoredProc;
    TransakcijaP: TpFIBTransaction;
    tblRabMestoVrabotenUcesnici: TpFIBDataSet;
    dsRabMestoVrabotenUcesnici: TDataSource;
    tblRabMestoVrabotenUcesniciID: TFIBIntegerField;
    tblRabMestoVrabotenUcesniciNAZIV: TFIBStringField;
    tblRabMestoVrabotenUcesniciSTUDIJA: TFIBIntegerField;
    tblRabMestoVrabotenUcesniciVRABOTEN: TFIBIntegerField;
    tblRabMestoVrabotenUcesniciIME_PREZIME: TFIBStringField;
    tblRabMestoVrabotenUcesniciBR_UCESNICI: TFIBSmallIntField;
    tblRabMestoVrabotenUcesniciCUSTOM1: TFIBStringField;
    tblRabMestoVrabotenUcesniciCUSTOM2: TFIBStringField;
    tblRabMestoVrabotenUcesniciCUSTOM3: TFIBStringField;
    tblRabMestoVrabotenUcesniciTS_INS: TFIBDateTimeField;
    tblRabMestoVrabotenUcesniciTS_UPD: TFIBDateTimeField;
    tblRabMestoVrabotenUcesniciUSR_INS: TFIBStringField;
    tblRabMestoVrabotenUcesniciUSR_UPD: TFIBStringField;
    tblLekovi: TpFIBDataSet;
    dsLekovi: TDataSource;
    tblLekoviID: TFIBIntegerField;
    tblLekoviNAZIV: TFIBStringField;
    tblLekoviCUSTOM1: TFIBStringField;
    tblLekoviCUSTOM2: TFIBStringField;
    tblLekoviCUSTOM3: TFIBStringField;
    tblLekoviTS_INS: TFIBDateTimeField;
    tblLekoviTS_UPD: TFIBDateTimeField;
    tblLekoviUSR_INS: TFIBStringField;
    tblLekoviUSR_UPD: TFIBStringField;
    tblStudijaLekovi: TpFIBDataSet;
    dsStudijaLekovi: TDataSource;
    PROC_KS_STUDIJA_LEKOVI: TpFIBStoredProc;
    tblStudijaLekoviID: TFIBIntegerField;
    tblStudijaLekoviSTUDIJA_ID: TFIBIntegerField;
    tblStudijaLekoviLEK_ID: TFIBIntegerField;
    tblStudijaLekoviLEK_NAZIV: TFIBStringField;
    tblStudijaLekoviPRIMEROK_KOLICINA: TFIBSmallIntField;
    tblStudijaLekoviCUSTOM1: TFIBStringField;
    tblStudijaLekoviCUSTOM2: TFIBStringField;
    tblStudijaLekoviCUSTOM3: TFIBStringField;
    tblStudijaLekoviTS_INS: TFIBDateTimeField;
    tblStudijaLekoviTS_UPD: TFIBDateTimeField;
    tblStudijaLekoviUSR_INS: TFIBStringField;
    tblStudijaLekoviUSR_UPD: TFIBStringField;
    PROC_KS_STUDIJA_PART: TpFIBStoredProc;
    tblStudijaPart: TpFIBDataSet;
    dsStudijaPart: TDataSource;
    tblStudijaPartID: TFIBIntegerField;
    tblStudijaPartSTUDIJA: TFIBIntegerField;
    tblStudijaPartPOCETEN_DATUM: TFIBDateField;
    tblStudijaPartBROJ_TOCKI: TFIBIntegerField;
    tblStudijaPartCUSTOM1: TFIBStringField;
    tblStudijaPartCUSTOM2: TFIBStringField;
    tblStudijaPartCUSTOM3: TFIBStringField;
    tblStudijaPartTS_INS: TFIBDateTimeField;
    tblStudijaPartTS_UPD: TFIBDateTimeField;
    tblStudijaPartUSR_INS: TFIBStringField;
    tblStudijaPartUSR_UPD: TFIBStringField;
    tblStudijaPartPART: TFIBStringField;
    tblPartTocki: TpFIBDataSet;
    dsPartTocki: TDataSource;
    tblPartTockiID: TFIBIntegerField;
    tblPartTockiPART_ID: TFIBIntegerField;
    tblPartTockiPRIMEROK_KOLICINA: TFIBSmallIntField;
    tblPartTockiCUSTOM1: TFIBStringField;
    tblPartTockiCUSTOM2: TFIBStringField;
    tblPartTockiCUSTOM3: TFIBStringField;
    tblPartTockiTS_INS: TFIBDateTimeField;
    tblPartTockiTS_UPD: TFIBDateTimeField;
    tblPartTockiUSR_INS: TFIBStringField;
    tblPartTockiUSR_UPD: TFIBStringField;
    tblStudijaPartPOCETNO_VREME: TFIBTimeField;
    PROC_KS_PART_TOCKI: TpFIBStoredProc;
    PROC_KS_GENERIRAJ_PRIMEROCI: TpFIBStoredProc;
    tblPrimeroci: TpFIBDataSet;
    dsPrimeroci: TDataSource;
    tblPrimerociID: TFIBIntegerField;
    tblPrimerociSTUDIJA: TFIBIntegerField;
    tblPrimerociRAB_MESTO: TFIBIntegerField;
    tblPrimerociRAB_MESTO_NAZIV: TFIBStringField;
    tblPrimerociPART: TFIBIntegerField;
    tblPrimerociPART_NAZIV: TFIBStringField;
    tblPrimerociDATUM: TFIBDateField;
    tblPrimerociVREME: TFIBTimeField;
    tblPrimerociREDEN_BROJ: TFIBIntegerField;
    tblPrimerociBROJ_EPRUVETA: TFIBStringField;
    tblPrimerociVREME_REALIZACIJA: TFIBTimeField;
    tblPrimerociCUSTOM1: TFIBStringField;
    tblPrimerociCUSTOM2: TFIBStringField;
    tblPrimerociCUSTOM3: TFIBStringField;
    tblPrimerociTS_INS: TFIBDateTimeField;
    tblPrimerociTS_UPD: TFIBDateTimeField;
    tblPrimerociUSR_INS: TFIBStringField;
    tblPrimerociUSR_UPD: TFIBStringField;
    qCountPrimeroci: TpFIBQuery;
    tblIzvestajPart: TpFIBDataSet;
    dsIzvestajPart: TDataSource;
    tblIzvestajPartID: TFIBIntegerField;
    tblIzvestajPartPART: TFIBStringField;
    tblIzvestajRabMesto: TpFIBDataSet;
    dsIzvestajRabMesto: TDataSource;
    tblIzvestajRabMestoID: TFIBIntegerField;
    tblIzvestajRabMestoNAZIV: TFIBStringField;
    tblIzvestajVremenskiTocki: TpFIBDataSet;
    dslIzvestajVremenskiTocki: TDataSource;
    tblIzvestajVremenskiTockiID: TFIBIntegerField;
    StyleRepository: TcxStyleRepository;
    Sunny: TcxStyle;
    Dark: TcxStyle;
    Golden: TcxStyle;
    Summer: TcxStyle;
    Autumn: TcxStyle;
    Bright: TcxStyle;
    Cold: TcxStyle;
    Spring: TcxStyle;
    Light: TcxStyle;
    Winter: TcxStyle;
    Title: TcxStyle;
    Search: TcxStyle;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    zContent: TcxStyle;
    zHeader: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    stlCheckBox: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;
    cxStyle33: TcxStyle;
    cxStyle34: TcxStyle;
    cxStyle35: TcxStyle;
    cxStyle36: TcxStyle;
    cxStyle37: TcxStyle;
    cxStyle38: TcxStyle;
    cxStyle39: TcxStyle;
    cxStyle40: TcxStyle;
    cxStyle41: TcxStyle;
    cxStyle42: TcxStyle;
    cxStyle43: TcxStyle;
    cxStyle44: TcxStyle;
    cxStyle45: TcxStyle;
    cxStyle46: TcxStyle;
    cxStyle47: TcxStyle;
    cxStyle48: TcxStyle;
    cxStyle49: TcxStyle;
    cxStyle50: TcxStyle;
    cxStyle51: TcxStyle;
    cxStyle52: TcxStyle;
    cxStyle53: TcxStyle;
    cxStyle54: TcxStyle;
    cxStyle55: TcxStyle;
    cxStyle56: TcxStyle;
    cxStyle57: TcxStyle;
    cxStyle58: TcxStyle;
    cxStyle59: TcxStyle;
    cxStyle60: TcxStyle;
    cxStyle61: TcxStyle;
    cxStyle62: TcxStyle;
    cxStyle63: TcxStyle;
    cxStyle64: TcxStyle;
    cxStyle65: TcxStyle;
    cxStyle66: TcxStyle;
    cxStyle67: TcxStyle;
    cxStyle68: TcxStyle;
    cxStyle69: TcxStyle;
    cxStyle70: TcxStyle;
    cxStyle71: TcxStyle;
    cxStyle72: TcxStyle;
    cxStyle73: TcxStyle;
    cxStyle74: TcxStyle;
    cxStyle75: TcxStyle;
    cxStyle76: TcxStyle;
    cxStyle77: TcxStyle;
    cxStyle78: TcxStyle;
    cxStyle79: TcxStyle;
    cxStyle80: TcxStyle;
    cxStyle81: TcxStyle;
    cxStyle82: TcxStyle;
    cxStyle83: TcxStyle;
    cxStyle84: TcxStyle;
    cxStyle85: TcxStyle;
    cxStyle86: TcxStyle;
    cxStyle87: TcxStyle;
    cxStyle88: TcxStyle;
    cxStyle89: TcxStyle;
    cxStyle90: TcxStyle;
    cxStyle91: TcxStyle;
    cxStyle92: TcxStyle;
    cxStyle93: TcxStyle;
    cxStyle94: TcxStyle;
    cxStyle95: TcxStyle;
    cxStyle96: TcxStyle;
    cxStyle97: TcxStyle;
    cxStyle98: TcxStyle;
    cxStyle99: TcxStyle;
    cxStyle100: TcxStyle;
    cxStyle101: TcxStyle;
    cxStyle102: TcxStyle;
    cxStyle103: TcxStyle;
    cxStyle104: TcxStyle;
    cxStyle105: TcxStyle;
    cxStyle106: TcxStyle;
    cxStyle107: TcxStyle;
    cxStyle108: TcxStyle;
    cxStyle109: TcxStyle;
    cxStyle110: TcxStyle;
    cxStyle111: TcxStyle;
    cxStyle112: TcxStyle;
    cxStyle113: TcxStyle;
    cxStyle114: TcxStyle;
    cxStyle115: TcxStyle;
    cxStyle116: TcxStyle;
    cxStyle117: TcxStyle;
    cxStyle118: TcxStyle;
    cxStyle119: TcxStyle;
    cxStyle120: TcxStyle;
    cxStyle121: TcxStyle;
    cxStyle122: TcxStyle;
    cxStyle123: TcxStyle;
    cxStyle124: TcxStyle;
    cxStyle125: TcxStyle;
    cxStyle126: TcxStyle;
    cxStyle127: TcxStyle;
    cxStyle128: TcxStyle;
    cxStyle129: TcxStyle;
    cxStyle130: TcxStyle;
    cxStyle131: TcxStyle;
    cxStyle132: TcxStyle;
    cxStyle133: TcxStyle;
    cxStyle134: TcxStyle;
    cxStyle135: TcxStyle;
    cxStyle136: TcxStyle;
    cxStyle137: TcxStyle;
    cxStyle138: TcxStyle;
    cxStyle139: TcxStyle;
    cxStyle140: TcxStyle;
    cxStyle141: TcxStyle;
    cxStyle142: TcxStyle;
    cxStyle143: TcxStyle;
    cxStyle144: TcxStyle;
    cxStyle145: TcxStyle;
    cxStyle146: TcxStyle;
    cxStyle147: TcxStyle;
    cxStyle148: TcxStyle;
    cxStyle149: TcxStyle;
    cxStyle150: TcxStyle;
    cxStyle151: TcxStyle;
    cxStyle152: TcxStyle;
    cxStyle153: TcxStyle;
    cxStyle154: TcxStyle;
    cxStyle155: TcxStyle;
    cxStyle156: TcxStyle;
    cxStyle157: TcxStyle;
    cxStyle158: TcxStyle;
    cxStyle159: TcxStyle;
    cxStyle160: TcxStyle;
    cxStyle161: TcxStyle;
    cxStyle162: TcxStyle;
    cxStyle163: TcxStyle;
    cxStyle164: TcxStyle;
    cxStyle165: TcxStyle;
    cxStyle166: TcxStyle;
    cxStyle167: TcxStyle;
    cxStyle168: TcxStyle;
    cxStyle169: TcxStyle;
    cxStyle170: TcxStyle;
    cxStyle171: TcxStyle;
    cxStyle172: TcxStyle;
    cxStyle173: TcxStyle;
    cxStyle174: TcxStyle;
    cxStyle175: TcxStyle;
    cxStyle176: TcxStyle;
    cxStyle177: TcxStyle;
    cxStyle178: TcxStyle;
    cxStyle179: TcxStyle;
    cxStyle180: TcxStyle;
    cxStyle181: TcxStyle;
    cxStyle182: TcxStyle;
    cxStyle183: TcxStyle;
    cxStyle184: TcxStyle;
    cxStyle185: TcxStyle;
    cxStyle186: TcxStyle;
    cxStyle187: TcxStyle;
    cxStyle188: TcxStyle;
    cxStyle189: TcxStyle;
    cxStyle190: TcxStyle;
    cxStyle191: TcxStyle;
    cxStyle192: TcxStyle;
    cxStyle193: TcxStyle;
    cxStyle194: TcxStyle;
    cxStyle195: TcxStyle;
    cxStyle196: TcxStyle;
    cxStyle197: TcxStyle;
    cxStyle198: TcxStyle;
    cxStyle199: TcxStyle;
    cxStyle200: TcxStyle;
    cxStyle201: TcxStyle;
    cxStyle202: TcxStyle;
    cxStyle203: TcxStyle;
    cxStyle204: TcxStyle;
    cxStyle205: TcxStyle;
    cxStyle206: TcxStyle;
    cxStyle207: TcxStyle;
    cxStyle208: TcxStyle;
    cxStyle209: TcxStyle;
    cxStyle210: TcxStyle;
    cxStyle211: TcxStyle;
    cxStyle212: TcxStyle;
    cxStyle213: TcxStyle;
    cxStyle227: TcxStyle;
    cxChartStyle: TcxStyle;
    cxStyle230: TcxStyle;
    RedLight: TcxStyle;
    MoneyGreen: TcxStyle;
    cxStyle232: TcxStyle;
    cxStyle233: TcxStyle;
    cxStyle234: TcxStyle;
    cxStyle235: TcxStyle;
    cxStyle236: TcxStyle;
    cxStyle237: TcxStyle;
    cxStyle238: TcxStyle;
    cxStyle239: TcxStyle;
    cxStyle240: TcxStyle;
    cxStyle241: TcxStyle;
    cxStyle242: TcxStyle;
    cxStyle243: TcxStyle;
    cxStyle244: TcxStyle;
    cxStyle245: TcxStyle;
    cxStyle246: TcxStyle;
    cxStyle247: TcxStyle;
    cxStyle248: TcxStyle;
    cxStyle249: TcxStyle;
    cxStyle250: TcxStyle;
    cxStyle251: TcxStyle;
    cxStyle252: TcxStyle;
    cxStyle253: TcxStyle;
    cxStyle254: TcxStyle;
    cxStyle255: TcxStyle;
    cxStyle256: TcxStyle;
    cxStyle257: TcxStyle;
    cxStyle258: TcxStyle;
    cxStyle259: TcxStyle;
    cxStyle260: TcxStyle;
    cxStyle261: TcxStyle;
    cxStyle262: TcxStyle;
    cxStyle263: TcxStyle;
    cxStyle264: TcxStyle;
    cxStyle265: TcxStyle;
    cxStyle266: TcxStyle;
    cxStyle267: TcxStyle;
    cxStyle268: TcxStyle;
    cxStyle269: TcxStyle;
    cxStyle270: TcxStyle;
    cxStyle271: TcxStyle;
    cxStyle272: TcxStyle;
    cxStyle273: TcxStyle;
    cxStyle274: TcxStyle;
    cxStyle275: TcxStyle;
    cxStyle276: TcxStyle;
    cxStyle277: TcxStyle;
    cxStyle278: TcxStyle;
    cxStyle279: TcxStyle;
    cxStyle280: TcxStyle;
    cxStyle281: TcxStyle;
    cxStyle282: TcxStyle;
    cxStyle283: TcxStyle;
    cxStyle284: TcxStyle;
    LightBlue: TcxStyle;
    BoldText: TcxStyle;
    UserStyleSheet: TcxGridTableViewStyleSheet;
    DetailStyleSheet: TcxGridTableViewStyleSheet;
    MasterStyleSheet: TcxGridTableViewStyleSheet;
    ssPrekuEdno: TcxGridTableViewStyleSheet;
    Banded: TcxGridBandedTableViewStyleSheet;
    TableViewDefaultStyle: TcxGridTableViewStyleSheet;
    GridBandedTableViewStyleSheetDevExpress: TcxGridBandedTableViewStyleSheet;
    GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet;
    TreeListStyleSheetDevExpress: TcxTreeListStyleSheet;
    PivotGridStyleSheetDevExpress: TcxPivotGridStyleSheet;
    GridTableViewStyleSheetLogin: TcxGridTableViewStyleSheet;
    dRed: TcxStyle;
    dGreen: TcxStyle;
    tblPrimerociTOCKA_BR: TFIBFloatField;
    tblVraboteniLOZINKA: TFIBStringField;
    tblVraboteniKORISNICKO_IME: TFIBStringField;
    tblVraboteniPRAVA: TFIBSmallIntField;
    tblPrimerociAKTIVEN_DOBROVOLEC: TFIBSmallIntField;
    PROC_KS_PROVERI_DEF_PARTOVI: TpFIBStoredProc;
    tblStudijaBR_REZERVI: TFIBIntegerField;
    tblRabMestoVrabotenUcesniciBR_REZERVI: TFIBIntegerField;
    tblStudijaRASTOJANIE: TFIBFloatField;
    tblStudijaPartTIP: TFIBSmallIntField;
    tblPrimerociDOBROVOLEC: TFIBStringField;
    tblStudijaCHEK_IN: TFIBSmallIntField;
    tblChekIn: TpFIBDataSet;
    dsChekIn: TDataSource;
    tblChekInID: TFIBIntegerField;
    tblChekInSTUDIJA: TFIBIntegerField;
    tblChekInRAB_MESTO: TFIBIntegerField;
    tblChekInRAB_MESTO_NAZIV: TFIBStringField;
    tblChekInPART: TFIBIntegerField;
    tblChekInPART_NAZIV: TFIBStringField;
    tblChekInDATUM: TFIBDateField;
    tblChekInVREME: TFIBTimeField;
    tblChekInDOBROVOLEC: TFIBStringField;
    tblChekInAKTIVEN_DOBROVOLEC: TFIBSmallIntField;
    tblChekInTOCKA_BR: TFIBFloatField;
    tblChekInREDEN_BROJ: TFIBIntegerField;
    tblChekInBROJ_EPRUVETA: TFIBStringField;
    tblChekInVREME_REALIZACIJA: TFIBTimeField;
    tblChekInCUSTOM1: TFIBStringField;
    tblChekInCUSTOM2: TFIBStringField;
    tblChekInCUSTOM3: TFIBStringField;
    tblChekInTS_INS: TFIBDateTimeField;
    tblChekInTS_UPD: TFIBDateTimeField;
    tblChekInUSR_INS: TFIBStringField;
    tblChekInUSR_UPD: TFIBStringField;
    tblStudijaREZERVI_NULTA_TOCKA: TFIBSmallIntField;
    tblStudijaCHEK_IN_RASTOJANIE: TFIBFloatField;
    tblBarkodDobrovolci: TpFIBDataSet;
    dsBarkodDobrovolci: TDataSource;
    tblBarkodDobrovolciKOD_STUDIJA: TFIBStringField;
    tblBarkodDobrovolciDOBROVOLEC: TFIBStringField;
    tblBarkodDobrovolciSTUDIJA_ID: TFIBStringField;
    tblBarkodDobrovolciBARCODE: TFIBStringField;
    tblBarkodDobrovolciDOBROVOLEC_ID: TFIBStringField;
    tblBarkodEpruveti: TpFIBDataSet;
    dsBarkodEpruveti: TDataSource;
    tblBarkodEpruvetiKOD_STUDIJA: TFIBStringField;
    tblBarkodEpruvetiDOBROVOLEC: TFIBStringField;
    tblBarkodEpruvetiSTUDIJA_ID: TFIBStringField;
    tblBarkodEpruvetiBARCODE: TFIBStringField;
    tblBarkodEpruvetiTOCKA_BR: TFIBFloatField;
    tblBarkodEpruvetiBROJ_EPRUVETA: TFIBStringField;
    tblBarkodEpruvetiBROJ_EPRUVETA_ID: TFIBStringField;
    tblBarkodEpruvetiPART: TFIBStringField;
    tblStudijaMINUTI_PLUS_MINUS: TFIBSmallIntField;
    qCountVremeRealizacija: TpFIBQuery;
    tblBarkodDobrovolciPecati: TpFIBDataSet;
    tblBarkodDobrovolciPecatiKOD_STUDIJA: TFIBStringField;
    tblBarkodDobrovolciPecatiDOBROVOLEC: TFIBStringField;
    tblBarkodDobrovolciPecatiSTUDIJA_ID: TFIBStringField;
    tblBarkodDobrovolciPecatiBARCODE: TFIBStringField;
    tblBarkodDobrovolciPecatiDOBROVOLEC_ID: TFIBStringField;
    tblBarkodEpruvetiPecati: TpFIBDataSet;
    tblBarkodEpruvetiPecatiRAB_MESTO: TFIBIntegerField;
    tblBarkodEpruvetiPecatiPAR_TIP: TFIBIntegerField;
    tblBarkodEpruvetiPecatiPART_ID: TFIBIntegerField;
    tblBarkodEpruvetiPecatiDOBROVOLEC: TFIBStringField;
    tblBarkodEpruvetiPecatiTOCKA_BR: TFIBFloatField;
    tblBarkodEpruvetiPecatiBROJ_EPRUVETA: TFIBStringField;
    tblBarkodEpruvetiPecatiPART: TFIBStringField;
    tblBarkodEpruvetiPecatiKOD_STUDIJA: TFIBStringField;
    tblBarkodEpruvetiPecatiSTUDIJA_ID: TFIBStringField;
    tblBarkodEpruvetiPecatiBARCODE: TFIBStringField;
    tblBarkodEpruvetiPecatiBROJ_EPRUVETA_ID: TFIBStringField;
    tblLogs: TpFIBDataSet;
    dsLogs: TDataSource;
    tblLogsID: TFIBIntegerField;
    tblLogsLOG_TAG: TFIBIntegerField;
    tblLogsKORISNICKO_IME: TFIBStringField;
    tblLogsOPIS: TFIBStringField;
    tblLogsVREME: TFIBStringField;
    tblLogsDATUM: TFIBStringField;
    tblLogsTS_INS: TFIBDateTimeField;
    tblLogsLOG_ID: TFIBIntegerField;
    tblLogsSTUDIJA_ID: TFIBIntegerField;
    tblLogsKOD_STUDIJA: TFIBStringField;
    tblLogsIME_PREZIME: TFIBStringField;
    tblLogslog_tag_naziv: TFIBStringField;
    qCountVrabotenRM: TpFIBQuery;
    tblBarkodEpruvetiDOBROVOLEC_ID: TFIBStringField;
    tblBarkodEpruvetiPecatiDOBROVOLEC_ID: TFIBStringField;
    tblPartTockiVREMENSKA_TOCKA: TFIBBCDField;
    tblChekInVREMENSKA_TOCKA: TFIBBCDField;
    tblPrimerociVREMENSKA_TOCKA: TFIBBCDField;
    tblIzvestajVremenskiTockiVREMENSKA_TOCKA: TFIBBCDField;
    tblChekInPRIMEROK_KOLICINA: TFIBBCDField;
    tblPrimerociPRIMEROK_KOLICINA: TFIBStringField;
    tblPartTockiBR_EPRUVETI: TFIBSmallIntField;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    procedure insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    procedure tblRabMestoVrabotenUcesniciIME_PREZIMESetText(Sender: TField;
      const Text: string);
    procedure tblRabMestoVrabotenUcesniciBR_UCESNICISetText(Sender: TField;
      const Text: string);
    procedure tblStudijaPartBROJ_TOCKISetText(Sender: TField;
      const Text: string);
    procedure tblPartTockiVREMENSKA_TOCKASetText(Sender: TField;
      const Text: string);
    procedure tblPartTockiPRIMEROK_KOLICINASetText(Sender: TField;
      const Text: string);
    procedure tblStudijaLekoviPRIMEROK_KOLICINASetText(Sender: TField;
      const Text: string);
    procedure tblStudijaPartPOCETNO_VREMESetText(Sender: TField;
      const Text: string);
    procedure tblStudijaPartPOCETEN_DATUMSetText(Sender: TField;
      const Text: string);
    function return3(Proc : TpFIBStoredProc; p1,  p2, p3, v1, v2, v3, broj: Variant):Variant;
    procedure tblRabMestoVrabotenUcesniciBR_REZERVISetText(Sender: TField;
      const Text: string);
  private
    { Private declarations }
  public
    { Public declarations }
   barcodePrinter, barcodeprint:string;
   brLabeliStudija, brlLabeliOpsto:integer;
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmKonekcija, DaNe;

{$R *.dfm}
procedure Tdm.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblPartTockiPRIMEROK_KOLICINASetText(Sender: TField;
  const Text: string);
begin

     tblPartTocki.Edit;
     tblPartTockiPRIMEROK_KOLICINA.Value:=StrToInt(Text);
     tblPartTocki.Post;
end;

procedure Tdm.tblPartTockiVREMENSKA_TOCKASetText(Sender: TField;
  const Text: string);
begin
     tblPartTocki.Edit;
     tblPartTockiVREMENSKA_TOCKA.Value:=StrToFloat(Text);
     tblPartTocki.Post;
end;

procedure Tdm.tblRabMestoVrabotenUcesniciBR_REZERVISetText(Sender: TField;
  const Text: string);
begin
     tblRabMestoVrabotenUcesnici.Edit;
     tblRabMestoVrabotenUcesniciBR_REZERVI.Value:=StrToInt(Text);
     tblRabMestoVrabotenUcesnici.Post;
end;

procedure Tdm.tblRabMestoVrabotenUcesniciBR_UCESNICISetText(Sender: TField;
  const Text: string);
begin
     tblRabMestoVrabotenUcesnici.Edit;
     tblRabMestoVrabotenUcesniciBR_UCESNICI.Value:=StrToInt(Text);
     tblRabMestoVrabotenUcesnici.Post;
end;

procedure Tdm.tblRabMestoVrabotenUcesniciIME_PREZIMESetText(Sender: TField;
  const Text: string);
begin
//     tblRabMestoVrabotenUcesnici.Edit;
//     tblRabMestoVrabotenUcesniciIME_PREZIME.Value:=Text;
//     tblRabMestoVrabotenUcesnici.Post;
end;

procedure Tdm.tblStudijaLekoviPRIMEROK_KOLICINASetText(Sender: TField;
  const Text: string);
begin
     dm.tblStudijaLekovi.Edit;
     dm.tblStudijaLekoviPRIMEROK_KOLICINA.Value:=StrToInt(Text);
     dm.tblStudijaLekovi.Post;
end;

procedure Tdm.tblStudijaPartBROJ_TOCKISetText(Sender: TField;
  const Text: string);
begin
     tblStudijaPart.Edit;
     tblStudijaPartBROJ_TOCKI.Value:=StrToInt(Text);
     tblStudijaPart.Post;
end;

procedure Tdm.tblStudijaPartPOCETEN_DATUMSetText(Sender: TField;
  const Text: string);
begin
      dm.tblStudijaPart.Edit;
      if (Text<>'') then
          dm.tblStudijaPartPOCETEN_DATUM.Value:=StrToDate(Text)
      else
          dm.tblStudijaPartPOCETEN_DATUM.Clear;
      dm.tblStudijaPart.Post;
end;

procedure Tdm.tblStudijaPartPOCETNO_VREMESetText(Sender: TField;
  const Text: string);
begin
      dm.tblStudijaPart.Edit;
      if (Text<>'') then
          dm.tblStudijaPartPOCETNO_VREME.Value:=StrToTime(Text)
      else
          dm.tblStudijaPartPOCETNO_VREME.Clear;
      dm.tblStudijaPart.Post;
end;

procedure Tdm.insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
    end;


function Tdm.return3(Proc : TpFIBStoredProc; p1,  p2, p3, v1, v2, v3, broj: Variant):Variant;
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;

end.
