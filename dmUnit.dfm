object dm: Tdm
  OldCreateOrder = False
  Height = 1089
  Width = 815
  object tblRabotnoMesto: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_RABOTNO_MESTO'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    STATUS = :STATUS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KS_RABOTNO_MESTO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KS_RABOTNO_MESTO('
      '    ID,'
      '    NAZIV,'
      '    STATUS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :STATUS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select ks.id,'
      '       ks.naziv,'
      '       ks.status,'
      '       ks.custom1,'
      '       ks.custom2,'
      '       ks.custom3,'
      '       ks.ts_ins,'
      '       ks.ts_upd,'
      '       ks.usr_ins,'
      '       ks.usr_upd'
      'from ks_rabotno_mesto ks'
      ''
      ' WHERE '
      '        KS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select ks.id,'
      '       ks.naziv,'
      '       ks.status,'
      '       ks.custom1,'
      '       ks.custom2,'
      '       ks.custom3,'
      '       ks.ts_ins,'
      '       ks.ts_upd,'
      '       ks.usr_ins,'
      '       ks.usr_upd'
      'from ks_rabotno_mesto ks'
      'order by ks.id')
    AutoUpdateOptions.UpdateTableName = 'KS_RABOTNO_MESTO'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_KS_RABOTNO_MESTO_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 64
    Top = 40
    object tblRabotnoMestoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRabotnoMestoNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblRabotnoMestoCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblRabotnoMestoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblRabotnoMestoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRabotnoMesto: TDataSource
    DataSet = tblRabotnoMesto
    Left = 200
    Top = 40
  end
  object tblNaracateli: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_NARACATEL'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KS_NARACATEL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KS_NARACATEL('
      '    ID,'
      '    NAZIV,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select n.id,'
      '       n.naziv,'
      '       n.custom1,'
      '       n.custom2,'
      '       n.custom3,'
      '       n.ts_ins,'
      '       n.ts_upd,'
      '       n.usr_ins,'
      '       n.usr_upd'
      'from ks_naracatel n'
      ''
      ' WHERE '
      '        N.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select n.id,'
      '       n.naziv,'
      '       n.custom1,'
      '       n.custom2,'
      '       n.custom3,'
      '       n.ts_ins,'
      '       n.ts_upd,'
      '       n.usr_ins,'
      '       n.usr_upd'
      'from ks_naracatel n')
    AutoUpdateOptions.UpdateTableName = 'KS_NARACATEL'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_KS_NARACATEL_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 56
    Top = 120
    object tblNaracateliID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblNaracateliNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNaracateliCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNaracateliCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNaracateliCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNaracateliTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblNaracateliTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblNaracateliUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNaracateliUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNaracateli: TDataSource
    DataSet = tblNaracateli
    Left = 208
    Top = 120
  end
  object tblStudija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_STUDIJA'
      'SET '
      '    ID = :ID,'
      '    STATUS = :STATUS,'
      '    KOD_STUDIJA = :KOD_STUDIJA,'
      '    DATUM = :DATUM,'
      '    NARACATEL = :NARACATEL,'
      '    OPIS = :OPIS,'
      '    BR_RAB_MESTA = :BR_RAB_MESTA,'
      '    BR_UCESNICI = :BR_UCESNICI,'
      '    BR_REZERVI = :BR_REZERVI,'
      '    REZERVI_NULTA_TOCKA = :REZERVI_NULTA_TOCKA,'
      '    PARTOVI = :PARTOVI,'
      '    RASTOJANIE = :RASTOJANIE,'
      '    CHEK_IN = :CHEK_IN,'
      '    CHEK_IN_RASTOJANIE = :CHEK_IN_RASTOJANIE,'
      '    MINUTI_PLUS_MINUS = :MINUTI_PLUS_MINUS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KS_STUDIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KS_STUDIJA('
      '    ID,'
      '    STATUS,'
      '    KOD_STUDIJA,'
      '    DATUM,'
      '    NARACATEL,'
      '    OPIS,'
      '    BR_RAB_MESTA,'
      '    BR_UCESNICI,'
      '    BR_REZERVI,'
      '    REZERVI_NULTA_TOCKA,'
      '    PARTOVI,'
      '    RASTOJANIE,'
      '    CHEK_IN,'
      '    CHEK_IN_RASTOJANIE,'
      '    MINUTI_PLUS_MINUS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :STATUS,'
      '    :KOD_STUDIJA,'
      '    :DATUM,'
      '    :NARACATEL,'
      '    :OPIS,'
      '    :BR_RAB_MESTA,'
      '    :BR_UCESNICI,'
      '    :BR_REZERVI,'
      '    :REZERVI_NULTA_TOCKA,'
      '    :PARTOVI,'
      '    :RASTOJANIE,'
      '    :CHEK_IN,'
      '    :CHEK_IN_RASTOJANIE,'
      '    :MINUTI_PLUS_MINUS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select s.id,'
      '       s.status,'
      '       s.kod_studija,'
      '       s.datum,'
      '       s.naracatel,'
      '       n.naziv as naracatelNaziv,'
      '       s.opis,'
      '       s.br_rab_mesta,'
      '       s.br_ucesnici,'
      '       s.br_rezervi,'
      '       s.rezervi_nulta_tocka,'
      '       s.partovi,'
      '       s.rastojanie,'
      '       s.chek_in,'
      '       s.chek_in_rastojanie,'
      '       s.minuti_plus_minus,'
      '       s.custom1,'
      '       s.custom2,'
      '       s.custom3,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd'
      'from ks_studija s'
      'left outer join ks_naracatel n on n.id = s.naracatel'
      ''
      ' WHERE '
      '        S.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select s.id,'
      '       s.status,'
      '       s.kod_studija,'
      '       s.datum,'
      '       s.naracatel,'
      '       n.naziv as naracatelNaziv,'
      '       s.opis,'
      '       s.br_rab_mesta,'
      '       s.br_ucesnici,'
      '       s.br_rezervi,'
      '       s.rezervi_nulta_tocka,'
      '       s.partovi,'
      '       s.rastojanie,'
      '       s.chek_in,'
      '       s.chek_in_rastojanie,'
      '       s.minuti_plus_minus,'
      '       s.custom1,'
      '       s.custom2,'
      '       s.custom3,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd'
      'from ks_studija s'
      'left outer join ks_naracatel n on n.id = s.naracatel')
    AutoUpdateOptions.UpdateTableName = 'KS_STUDIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_KS_STUDIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 56
    Top = 192
    object tblStudijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStudijaSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblStudijaKOD_STUDIJA: TFIBStringField
      DisplayLabel = #1050#1086#1076
      FieldName = 'KOD_STUDIJA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblStudijaNARACATEL: TFIBIntegerField
      DisplayLabel = #1053#1072#1088#1072#1095#1072#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'NARACATEL'
    end
    object tblStudijaNARACATELNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1088#1072#1095#1072#1090#1077#1083
      FieldName = 'NARACATELNAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaBR_RAB_MESTA: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
      FieldName = 'BR_RAB_MESTA'
    end
    object tblStudijaBR_UCESNICI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080
      FieldName = 'BR_UCESNICI'
    end
    object tblStudijaPARTOVI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1090#1086#1074#1080
      FieldName = 'PARTOVI'
    end
    object tblStudijaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblStudijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblStudijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaBR_REZERVI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1079#1077#1088#1074#1085#1080' '#1091#1095#1077#1089#1085#1080#1094#1080
      FieldName = 'BR_REZERVI'
    end
    object tblStudijaRASTOJANIE: TFIBFloatField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1079#1083#1080#1082#1072
      FieldName = 'RASTOJANIE'
    end
    object tblStudijaCHEK_IN: TFIBSmallIntField
      Alignment = taCenter
      DisplayLabel = 'Chek in'
      FieldName = 'CHEK_IN'
    end
    object tblStudijaREZERVI_NULTA_TOCKA: TFIBSmallIntField
      Alignment = taCenter
      DisplayLabel = #1047#1077#1084#1072#1114#1077' '#1082#1088#1074' '#1074#1086' '#1085#1091#1083#1090#1072' '#1090#1086#1095#1082#1072' '#1085#1072' '#1088#1077#1079#1077#1088#1074#1080
      FieldName = 'REZERVI_NULTA_TOCKA'
    end
    object tblStudijaCHEK_IN_RASTOJANIE: TFIBFloatField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1079#1083#1080#1082#1072' '#1074#1086' Chek in '#1083#1080#1089#1090#1072
      FieldName = 'CHEK_IN_RASTOJANIE'
    end
    object tblStudijaMINUTI_PLUS_MINUS: TFIBSmallIntField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1079#1083#1080#1082#1072' ('#1087#1083#1091#1089'/'#1084#1080#1085#1091#1089')'
      FieldName = 'MINUTI_PLUS_MINUS'
    end
  end
  object dsStudija: TDataSource
    DataSet = tblStudija
    Left = 216
    Top = 192
  end
  object tblVraboteni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_VRABOTENI'
      'SET '
      '    ID = :ID,'
      '    IME_PREZIME = :IME_PREZIME,'
      '    LOZINKA = :LOZINKA,'
      '    KORISNICKO_IME = :KORISNICKO_IME,'
      '    PRAVA = :PRAVA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KS_VRABOTENI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KS_VRABOTENI('
      '    ID,'
      '    IME_PREZIME,'
      '    LOZINKA,'
      '    KORISNICKO_IME,'
      '    PRAVA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :IME_PREZIME,'
      '    :LOZINKA,'
      '    :KORISNICKO_IME,'
      '    :PRAVA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select v.id,'
      '       v.ime_prezime,'
      '       v.lozinka,'
      '       v.korisnicko_ime,'
      '       v.prava,'
      '       v.custom1,'
      '       v.custom2,'
      '       v.custom3,'
      '       v.ts_ins,'
      '       v.ts_upd,'
      '       v.usr_ins,'
      '       v.usr_upd'
      'from ks_vraboteni v'
      ''
      ' WHERE '
      '        V.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select v.id,'
      '       v.ime_prezime,'
      '       v.lozinka,'
      '       v.korisnicko_ime,'
      '       v.prava,'
      '       v.custom1,'
      '       v.custom2,'
      '       v.custom3,'
      '       v.ts_ins,'
      '       v.ts_upd,'
      '       v.usr_ins,'
      '       v.usr_upd'
      'from ks_vraboteni v')
    AutoUpdateOptions.UpdateTableName = 'KS_VRABOTENI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_KS_VRABOTENI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 56
    Top = 264
    object tblVraboteniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblVraboteniIME_PREZIME: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'IME_PREZIME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblVraboteniTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblVraboteniUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniLOZINKA: TFIBStringField
      DisplayLabel = #1051#1086#1079#1080#1085#1082#1072
      FieldName = 'LOZINKA'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniKORISNICKO_IME: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1095#1082#1086' '#1080#1084#1077
      FieldName = 'KORISNICKO_IME'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniPRAVA: TFIBSmallIntField
      DisplayLabel = #1055#1088#1072#1074#1072' '#1079#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1084#1086#1073#1080#1083#1085#1072' '#1072#1087#1083#1080#1082#1072#1094#1080#1112#1072
      FieldName = 'PRAVA'
    end
  end
  object dsVraboteni: TDataSource
    DataSet = tblVraboteni
    Left = 208
    Top = 264
  end
  object PROC_KS_RABMESTA_VRAB_UCES: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_KS_RABMESTA_VRAB_UCES (?KS_ID, ?STATE)')
    StoredProcName = 'PROC_KS_RABMESTA_VRAB_UCES'
    Left = 664
    Top = 112
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 660
    Top = 48
  end
  object tblRabMestoVrabotenUcesnici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_RABOTNO_MESTO_VRABOTENI'
      'SET '
      '    VRABOTEN = :VRABOTEN,'
      '    BR_UCESNICI = :BR_UCESNICI,'
      '    BR_REZERVI = :BR_REZERVI'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select rv.id,'
      '       rv.naziv,'
      '       rv.studija,'
      '       rv.vraboten,'
      '       v.ime_prezime,'
      '       rv.br_ucesnici,'
      '       rv.br_rezervi,'
      '       rv.custom1,'
      '       rv.custom2,'
      '       rv.custom3,'
      '       rv.ts_ins,'
      '       rv.ts_upd,'
      '       rv.usr_ins,'
      '       rv.usr_upd'
      'from ks_rabotno_mesto_vraboteni rv'
      'left outer join ks_vraboteni v on v.id = rv.vraboten'
      'where(  rv.studija = :mas_id'
      '     ) and (     RV.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select rv.id,'
      '       rv.naziv,'
      '       rv.studija,'
      '       rv.vraboten,'
      '       v.ime_prezime,'
      '       rv.br_ucesnici,'
      '       rv.br_rezervi,'
      '       rv.custom1,'
      '       rv.custom2,'
      '       rv.custom3,'
      '       rv.ts_ins,'
      '       rv.ts_upd,'
      '       rv.usr_ins,'
      '       rv.usr_upd'
      'from ks_rabotno_mesto_vraboteni rv'
      'left outer join ks_vraboteni v on v.id = rv.vraboten'
      'where rv.studija = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsStudija
    Left = 56
    Top = 336
    object tblRabMestoVrabotenUcesniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRabMestoVrabotenUcesniciNAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabMestoVrabotenUcesniciSTUDIJA: TFIBIntegerField
      DisplayLabel = #1050#1083#1080#1085#1080#1095#1082#1072' '#1089#1090#1091#1076#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'STUDIJA'
    end
    object tblRabMestoVrabotenUcesniciVRABOTEN: TFIBIntegerField
      DisplayLabel = #1042#1088#1072#1073#1086#1090#1077#1085
      FieldName = 'VRABOTEN'
    end
    object tblRabMestoVrabotenUcesniciIME_PREZIME: TFIBStringField
      DisplayLabel = #1042#1088#1072#1073#1086#1090#1077#1085
      FieldName = 'IME_PREZIME'
      OnSetText = tblRabMestoVrabotenUcesniciIME_PREZIMESetText
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabMestoVrabotenUcesniciBR_UCESNICI: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080
      FieldName = 'BR_UCESNICI'
      OnSetText = tblRabMestoVrabotenUcesniciBR_UCESNICISetText
    end
    object tblRabMestoVrabotenUcesniciBR_REZERVI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1079#1077#1088#1074#1085#1080' '#1076#1086#1073#1088#1086#1074#1086#1083#1094#1080
      FieldName = 'BR_REZERVI'
      OnSetText = tblRabMestoVrabotenUcesniciBR_REZERVISetText
    end
    object tblRabMestoVrabotenUcesniciCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabMestoVrabotenUcesniciCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabMestoVrabotenUcesniciCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabMestoVrabotenUcesniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblRabMestoVrabotenUcesniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblRabMestoVrabotenUcesniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabMestoVrabotenUcesniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRabMestoVrabotenUcesnici: TDataSource
    DataSet = tblRabMestoVrabotenUcesnici
    Left = 216
    Top = 336
  end
  object tblLekovi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_LEKOVI'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KS_LEKOVI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KS_LEKOVI('
      '    ID,'
      '    NAZIV,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select l.id,'
      '       l.naziv,'
      '       l.custom1,'
      '       l.custom2,'
      '       l.custom3,'
      '       l.ts_ins,'
      '       l.ts_upd,'
      '       l.usr_ins,'
      '       l.usr_upd'
      'from ks_lekovi l'
      ''
      ' WHERE '
      '        L.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select l.id,'
      '       l.naziv,'
      '       l.custom1,'
      '       l.custom2,'
      '       l.custom3,'
      '       l.ts_ins,'
      '       l.ts_upd,'
      '       l.usr_ins,'
      '       l.usr_upd'
      'from ks_lekovi l')
    AutoUpdateOptions.UpdateTableName = 'KS_LEKOVI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_KS_LEKOVI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 56
    Top = 408
    object tblLekoviID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLekoviNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblLekoviTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblLekoviUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLekovi: TDataSource
    DataSet = tblLekovi
    Left = 208
    Top = 408
  end
  object tblStudijaLekovi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_STUDIJA_LEKOVI'
      'SET '
      '    PRIMEROK_KOLICINA = :PRIMEROK_KOLICINA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select kl.id,'
      '       kl.studija_id,'
      '       kl.lek_id,'
      '       l.naziv as lek_naziv,'
      '       kl.primerok_kolicina,'
      '       kl.custom1,'
      '       kl.custom2,'
      '       kl.custom3,'
      '       kl.ts_ins,'
      '       kl.ts_upd,'
      '       kl.usr_ins,'
      '       kl.usr_upd'
      'from ks_studija_lekovi kl'
      'inner join ks_lekovi l on l.id = kl.lek_id'
      'where kl.studija_id = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsStudija
    Left = 56
    Top = 472
    object tblStudijaLekoviID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStudijaLekoviSTUDIJA_ID: TFIBIntegerField
      DisplayLabel = #1057#1090#1091#1076#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'STUDIJA_ID'
    end
    object tblStudijaLekoviLEK_ID: TFIBIntegerField
      DisplayLabel = #1051#1077#1082' - '#1096#1080#1092#1088#1072
      FieldName = 'LEK_ID'
    end
    object tblStudijaLekoviLEK_NAZIV: TFIBStringField
      DisplayLabel = #1051#1077#1082
      FieldName = 'LEK_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaLekoviPRIMEROK_KOLICINA: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082
      FieldName = 'PRIMEROK_KOLICINA'
      OnSetText = tblStudijaLekoviPRIMEROK_KOLICINASetText
    end
    object tblStudijaLekoviCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaLekoviCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaLekoviCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaLekoviTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblStudijaLekoviTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblStudijaLekoviUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaLekoviUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStudijaLekovi: TDataSource
    DataSet = tblStudijaLekovi
    Left = 208
    Top = 472
  end
  object PROC_KS_STUDIJA_LEKOVI: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_KS_STUDIJA_LEKOVI (?KS_ID, ?LEK_ID, ?TAG)')
    StoredProcName = 'PROC_KS_STUDIJA_LEKOVI'
    Left = 664
    Top = 176
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object PROC_KS_STUDIJA_PART: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_KS_STUDIJA_PART (?KS_ID, ?PART_BR, ?STATE' +
        ')')
    StoredProcName = 'PROC_KS_STUDIJA_PART'
    Left = 664
    Top = 248
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblStudijaPart: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_PARTOVI'
      'SET '
      ''
      '    POCETEN_DATUM = :POCETEN_DATUM,'
      '    POCETNO_VREME = :POCETNO_VREME,'
      '    BROJ_TOCKI = :BROJ_TOCKI'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.studija,'
      '       p.part,'
      '       p.poceten_datum,'
      '       p.pocetno_vreme,'
      '       p.broj_tocki,'
      '       p.tip,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from ks_partovi p'
      'where p.studija = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsStudija
    Left = 56
    Top = 544
    object tblStudijaPartID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStudijaPartSTUDIJA: TFIBIntegerField
      DisplayLabel = #1057#1090#1091#1076#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'STUDIJA'
    end
    object tblStudijaPartPART: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090
      FieldName = 'PART'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaPartPOCETNO_VREME: TFIBTimeField
      DisplayLabel = #1055#1086#1095#1077#1090#1085#1086' '#1074#1088#1077#1084#1077
      FieldName = 'POCETNO_VREME'
      OnSetText = tblStudijaPartPOCETNO_VREMESetText
      DisplayFormat = 'hh:mm'
    end
    object tblStudijaPartPOCETEN_DATUM: TFIBDateField
      DisplayLabel = #1055#1086#1095#1077#1090#1077#1085' '#1076#1072#1090#1091#1084
      FieldName = 'POCETEN_DATUM'
      OnSetText = tblStudijaPartPOCETEN_DATUMSetText
    end
    object tblStudijaPartBROJ_TOCKI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1074#1088#1077#1084#1077#1085#1089#1082#1080' '#1090#1086#1095#1082#1080
      FieldName = 'BROJ_TOCKI'
      OnSetText = tblStudijaPartBROJ_TOCKISetText
    end
    object tblStudijaPartCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaPartCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaPartCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaPartTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblStudijaPartTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblStudijaPartUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaPartUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStudijaPartTIP: TFIBSmallIntField
      FieldName = 'TIP'
    end
  end
  object dsStudijaPart: TDataSource
    DataSet = tblStudijaPart
    Left = 200
    Top = 544
  end
  object tblPartTocki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_PART_TOCKI'
      'SET '
      ''
      '    PRIMEROK_KOLICINA = :PRIMEROK_KOLICINA,'
      '    VREMENSKA_TOCKA = :VREMENSKA_TOCKA ,'
      '    br_epruveti = :br_epruveti'
      '   '
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select pt.id,'
      '       pt.part_id,'
      '       pt.primerok_kolicina,'
      '       pt.vremenska_tocka,'
      '       pt.custom1,'
      '       pt.custom2,'
      '       pt.custom3,'
      '       pt.ts_ins,'
      '       pt.ts_upd,'
      '       pt.usr_ins,'
      '       pt.usr_upd,'
      '       pt.br_epruveti'
      'from ks_part_tocki pt'
      'where(  pt.part_id = :mas_id'
      '     ) and (     PT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pt.id,'
      '       pt.part_id,'
      '       pt.primerok_kolicina,'
      '       pt.vremenska_tocka,'
      '       pt.custom1,'
      '       pt.custom2,'
      '       pt.custom3,'
      '       pt.ts_ins,'
      '       pt.ts_upd,'
      '       pt.usr_ins,'
      '       pt.usr_upd  ,'
      '       pt.br_epruveti'
      'from ks_part_tocki pt'
      'where pt.part_id = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsStudijaPart
    Left = 48
    Top = 603
    object tblPartTockiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPartTockiPART_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PART_ID'
    end
    object tblPartTockiPRIMEROK_KOLICINA: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082
      FieldName = 'PRIMEROK_KOLICINA'
      OnSetText = tblPartTockiPRIMEROK_KOLICINASetText
    end
    object tblPartTockiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartTockiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartTockiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartTockiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPartTockiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPartTockiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartTockiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartTockiVREMENSKA_TOCKA: TFIBBCDField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1086' '#1088#1072#1089#1090#1086#1112#1072#1085#1080#1077' '#1086#1076' '#1085#1091#1083#1090#1072' '#1090#1086#1095#1082#1072
      FieldName = 'VREMENSKA_TOCKA'
      Size = 3
    end
    object tblPartTockiBR_EPRUVETI: TFIBSmallIntField
      FieldName = 'BR_EPRUVETI'
    end
  end
  object dsPartTocki: TDataSource
    DataSet = tblPartTocki
    Left = 200
    Top = 608
  end
  object PROC_KS_PART_TOCKI: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_KS_PART_TOCKI (?PART_ID, ?KS_ID)')
    StoredProcName = 'PROC_KS_PART_TOCKI'
    Left = 672
    Top = 320
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object PROC_KS_GENERIRAJ_PRIMEROCI: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_KS_GENERIRAJ_PRIMEROCI (?KS_ID)')
    StoredProcName = 'PROC_KS_GENERIRAJ_PRIMEROCI'
    Left = 672
    Top = 384
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblPrimeroci: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KS_PRIMEROCI'
      'SET '
      '    AKTIVEN_DOBROVOLEC = :AKTIVEN_DOBROVOLEC'
      'WHERE'
      
        '    ID >= :OLD_ID and DOBROVOLEC = :OLD_DOBROVOLEC and STUDIJA =' +
        ' :OLD_STUDIJA'
      '    ')
    DeleteSQL.Strings = (
      ''
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.studija,'
      '       p.rab_mesto,'
      '       rmv.naziv as rab_mesto_naziv,'
      '       p.part,'
      '       kp.part as part_naziv,'
      '       p.datum,'
      '       p.vreme,'
      '       p.dobrovolec,'
      '       p.aktiven_dobrovolec,'
      '       p.tocka_br,'
      '       pt.vremenska_tocka,'
      
        '       case when kp.tip = -2 then (select  sum(sl.primerok_kolic' +
        'ina)from ks_studija_lekovi sl where sl.studija_id = :mas_id)||'#39' ' +
        'ml krv '#39
      '            when kp.tip = -1 then  '#39' '#39
      '            else pt.primerok_kolicina||'#39' ml krv '#39
      '       end "PRIMEROK_KOLICINA",'
      '       p.reden_broj,'
      '       p.broj_epruveta,'
      '       p.vreme_realizacija,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from ks_primeroci p'
      'inner join ks_part_tocki pt on pt.id = p.part'
      'inner join ks_partovi kp on kp.id = pt.part_id'
      
        'inner join ks_rabotno_mesto_vraboteni rmv on rmv.id = p.rab_mest' +
        'o'
      'where p.studija = :mas_id   and kp.tip <> - 3'
      'order by p.rab_mesto,kp.id, pt.vremenska_tocka, p.dobrovolec')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsStudija
    Left = 48
    Top = 672
    object tblPrimerociID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPrimerociSTUDIJA: TFIBIntegerField
      DisplayLabel = #1057#1090#1091#1076#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'STUDIJA'
    end
    object tblPrimerociRAB_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RAB_MESTO'
    end
    object tblPrimerociRAB_MESTO_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RAB_MESTO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociPART: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PART'
    end
    object tblPrimerociPART_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090
      FieldName = 'PART_NAZIV'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblPrimerociVREME: TFIBTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
      DisplayFormat = 'hh:mm:ss'
    end
    object tblPrimerociREDEN_BROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1077#1087#1088#1091#1074#1077#1090#1072
      FieldName = 'REDEN_BROJ'
    end
    object tblPrimerociBROJ_EPRUVETA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1077#1087#1088#1091#1074#1077#1090#1072
      FieldName = 'BROJ_EPRUVETA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociVREME_REALIZACIJA: TFIBTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'VREME_REALIZACIJA'
      DisplayFormat = 'hh:mm:ss'
    end
    object tblPrimerociCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPrimerociTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPrimerociUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociTOCKA_BR: TFIBFloatField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1090#1086#1095#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'TOCKA_BR'
    end
    object tblPrimerociAKTIVEN_DOBROVOLEC: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1076#1086#1073#1088#1086#1074#1086#1083#1077#1094
      FieldName = 'AKTIVEN_DOBROVOLEC'
    end
    object tblPrimerociDOBROVOLEC: TFIBStringField
      DisplayLabel = #1044#1086#1073#1088#1086#1074#1086#1083#1077#1094
      FieldName = 'DOBROVOLEC'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrimerociVREMENSKA_TOCKA: TFIBBCDField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1090#1086#1095#1082#1072
      FieldName = 'VREMENSKA_TOCKA'
      Size = 3
    end
    object tblPrimerociPRIMEROK_KOLICINA: TFIBStringField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082' ('#1084#1083')'
      FieldName = 'PRIMEROK_KOLICINA'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPrimeroci: TDataSource
    DataSet = tblPrimeroci
    Left = 200
    Top = 659
  end
  object qCountPrimeroci: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(s.id) as br'
      'from  ks_primeroci s'
      'where s.studija = :ks_id')
    Left = 448
    Top = 56
  end
  object tblIzvestajPart: TpFIBDataSet
    SelectSQL.Strings = (
      'select  kp.id, kp.part'
      'from ks_partovi kp'
      'where kp.studija = :ks_studija')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 744
    object tblIzvestajPartID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzvestajPartPART: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090
      FieldName = 'PART'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzvestajPart: TDataSource
    DataSet = tblIzvestajPart
    Left = 192
    Top = 744
  end
  object tblIzvestajRabMesto: TpFIBDataSet
    SelectSQL.Strings = (
      'select  rv.id, rv.naziv'
      'from ks_rabotno_mesto_vraboteni rv'
      'where rv.studija = :ks_studija')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 816
    object tblIzvestajRabMestoID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzvestajRabMestoNAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzvestajRabMesto: TDataSource
    DataSet = tblIzvestajRabMesto
    Left = 192
    Top = 816
  end
  object tblIzvestajVremenskiTocki: TpFIBDataSet
    SelectSQL.Strings = (
      'select  pt.vremenska_tocka, pt.id'
      'from ks_part_tocki pt'
      'inner join ks_partovi p on p.id=pt.part_id'
      'where  pt.part_id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 880
    object tblIzvestajVremenskiTockiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzvestajVremenskiTockiVREMENSKA_TOCKA: TFIBBCDField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1090#1086#1095#1082#1072
      FieldName = 'VREMENSKA_TOCKA'
      Size = 3
    end
  end
  object dslIzvestajVremenskiTocki: TDataSource
    DataSet = tblIzvestajVremenskiTocki
    Left = 192
    Top = 880
  end
  object StyleRepository: TcxStyleRepository
    Left = 552
    Top = 48
    PixelsPerInch = 96
    object Sunny: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clNavy
    end
    object Dark: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWhite
    end
    object Golden: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 4707838
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object Summer: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Autumn: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object Bright: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 16749885
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Cold: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Spring: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Light: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object Winter: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Title: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clNavy
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object Search: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clRed
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clCream
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 4707838
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12615680
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clRed
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clNavy
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clYellow
    end
    object zContent: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clMoneyGreen
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object zHeader: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clTeal
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWhite
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor]
      Color = 15593713
    end
    object cxStyle27: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle28: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle29: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle30: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object stlCheckBox: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle31: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clMoneyGreen
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle32: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle33: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle34: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle35: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle36: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle37: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle38: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle39: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle40: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle41: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle42: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle43: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle44: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle45: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle46: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle47: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle48: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle49: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle50: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle51: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle52: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle53: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 4707838
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle54: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle55: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle56: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle57: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle58: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle59: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle60: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle61: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle62: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle63: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle64: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle65: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle66: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle67: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle68: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle69: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle70: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle71: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle72: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle73: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle74: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle75: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle76: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle77: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle78: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle79: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle80: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle81: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle82: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle83: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle84: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle85: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle86: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle87: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle88: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle89: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle90: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle91: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle92: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle93: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle94: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle95: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle96: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle97: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle98: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle99: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle100: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle101: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle102: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle103: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle104: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle105: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle106: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle107: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle108: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle109: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle110: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle111: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle112: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle113: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle114: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle115: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle116: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle117: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle118: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle119: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle120: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle121: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle122: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle123: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle124: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle125: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle126: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle127: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle128: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle129: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle130: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle131: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle132: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle133: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle134: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle135: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle136: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle137: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle138: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle139: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle140: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle141: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle142: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle143: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle144: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle145: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle146: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle147: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle148: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle149: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle150: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle151: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle152: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle153: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle154: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle155: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle156: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle157: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle158: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle159: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle160: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle161: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle162: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle163: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle164: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle165: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle166: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle167: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle168: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16749885
      TextColor = clWhite
    end
    object cxStyle169: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle170: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle171: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle172: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle173: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle174: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle175: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle176: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle177: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle178: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle179: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle180: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle181: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle182: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle183: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle184: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle185: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle186: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle187: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle188: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle189: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle190: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle191: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle192: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle193: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle194: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle195: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle196: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle197: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle198: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle199: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle200: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle201: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle202: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle203: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle204: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle205: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle206: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle207: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle208: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle209: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle210: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle211: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle212: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      TextColor = clBlack
    end
    object cxStyle213: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 13816275
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle227: TcxStyle
    end
    object cxChartStyle: TcxStyle
      AssignedValues = [svColor]
      Color = 13149574
    end
    object cxStyle230: TcxStyle
      AssignedValues = [svColor]
      Color = 15389113
    end
    object RedLight: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 8421631
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object MoneyGreen: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
    object cxStyle232: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle233: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle234: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle235: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle236: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle237: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle238: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle239: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle240: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle241: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle242: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle243: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle244: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle245: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle246: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle247: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle248: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle249: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle250: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle251: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle252: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle253: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle254: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle255: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle256: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle257: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle258: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle259: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle260: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle261: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle262: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle263: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle264: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle265: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle266: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle267: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle268: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle269: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle270: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle271: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle272: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle273: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle274: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle275: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle276: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle277: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle278: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 4707838
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle279: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle280: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle281: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle282: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle283: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle284: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWhite
    end
    object LightBlue: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14405055
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object BoldText: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object dRed: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clDefault
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clRed
    end
    object dGreen: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clDefault
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clGreen
    end
    object UserStyleSheet: TcxGridTableViewStyleSheet
      Caption = 'User Defined Style Sheet'
      Styles.Background = Dark
      Styles.Content = Spring
      Styles.FilterBox = Sunny
      Styles.Footer = Light
      Styles.Group = Cold
      Styles.GroupByBox = Golden
      Styles.Inactive = Summer
      Styles.Indicator = Autumn
      Styles.Preview = Winter
      Styles.Selection = Bright
      BuiltIn = True
    end
    object DetailStyleSheet: TcxGridTableViewStyleSheet
      Caption = 'Zoko'
      Styles.Background = cxStyle1
      Styles.Content = zContent
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.IncSearch = cxStyle11
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = zHeader
      Styles.Inactive = cxStyle10
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      Styles.Selection = cxStyle14
      BuiltIn = True
    end
    object MasterStyleSheet: TcxGridTableViewStyleSheet
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.IncSearch = cxStyle11
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Inactive = cxStyle10
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      Styles.Selection = cxStyle14
      BuiltIn = True
    end
    object ssPrekuEdno: TcxGridTableViewStyleSheet
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = Light
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.IncSearch = cxStyle11
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Inactive = cxStyle10
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      Styles.Selection = cxStyle14
      BuiltIn = True
    end
    object Banded: TcxGridBandedTableViewStyleSheet
      Caption = 'DevExpress Banded'
      Styles.Background = cxStyle15
      Styles.Content = cxStyle18
      Styles.ContentEven = cxStyle19
      Styles.ContentOdd = cxStyle20
      Styles.FilterBox = cxStyle21
      Styles.IncSearch = cxStyle27
      Styles.Footer = cxStyle22
      Styles.Group = cxStyle23
      Styles.GroupByBox = cxStyle24
      Styles.Header = cxStyle25
      Styles.Inactive = cxStyle26
      Styles.Indicator = cxStyle28
      Styles.Preview = cxStyle29
      Styles.Selection = cxStyle30
      Styles.BandBackground = cxStyle16
      Styles.BandHeader = cxStyle17
      BuiltIn = True
    end
    object TableViewDefaultStyle: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle116
      Styles.Content = cxStyle117
      Styles.ContentEven = cxStyle118
      Styles.ContentOdd = cxStyle119
      Styles.FilterBox = cxStyle120
      Styles.IncSearch = cxStyle126
      Styles.Footer = cxStyle121
      Styles.Group = cxStyle122
      Styles.GroupByBox = cxStyle123
      Styles.Header = cxStyle124
      Styles.Inactive = cxStyle125
      Styles.Indicator = cxStyle127
      Styles.Preview = cxStyle128
      Styles.Selection = cxStyle129
      BuiltIn = True
    end
    object GridBandedTableViewStyleSheetDevExpress: TcxGridBandedTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle144
      Styles.Content = cxStyle147
      Styles.ContentEven = cxStyle148
      Styles.ContentOdd = cxStyle149
      Styles.FilterBox = cxStyle150
      Styles.IncSearch = cxStyle156
      Styles.Footer = cxStyle151
      Styles.Group = cxStyle152
      Styles.GroupByBox = cxStyle153
      Styles.Header = cxStyle154
      Styles.Inactive = cxStyle155
      Styles.Indicator = cxStyle157
      Styles.Preview = cxStyle158
      Styles.Selection = cxStyle159
      Styles.BandBackground = cxStyle145
      Styles.BandHeader = cxStyle146
      BuiltIn = True
    end
    object GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle160
      Styles.Content = cxStyle163
      Styles.ContentEven = cxStyle164
      Styles.ContentOdd = cxStyle165
      Styles.IncSearch = cxStyle167
      Styles.CaptionRow = cxStyle161
      Styles.CardBorder = cxStyle162
      Styles.Inactive = cxStyle166
      Styles.RowCaption = cxStyle168
      Styles.Selection = cxStyle169
      BuiltIn = True
    end
    object TreeListStyleSheetDevExpress: TcxTreeListStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle170
      Styles.Content = cxStyle174
      Styles.Inactive = cxStyle178
      Styles.Selection = cxStyle182
      Styles.BandBackground = cxStyle171
      Styles.BandHeader = cxStyle172
      Styles.ColumnHeader = cxStyle173
      Styles.ContentEven = cxStyle175
      Styles.ContentOdd = cxStyle176
      Styles.Footer = cxStyle177
      Styles.IncSearch = cxStyle179
      Styles.Indicator = cxStyle180
      Styles.Preview = cxStyle181
      BuiltIn = True
    end
    object PivotGridStyleSheetDevExpress: TcxPivotGridStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle183
      Styles.ColumnHeader = cxStyle184
      Styles.Content = cxStyle185
      Styles.FieldHeader = cxStyle186
      Styles.HeaderBackground = cxStyle187
      Styles.Inactive = cxStyle188
      Styles.Prefilter = cxStyle189
      Styles.RowHeader = cxStyle190
      Styles.Selected = cxStyle191
      BuiltIn = True
    end
    object GridTableViewStyleSheetLogin: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle271
      Styles.Content = cxStyle272
      Styles.ContentEven = cxStyle273
      Styles.ContentOdd = cxStyle274
      Styles.FilterBox = cxStyle275
      Styles.IncSearch = cxStyle281
      Styles.Footer = cxStyle276
      Styles.Group = cxStyle277
      Styles.GroupByBox = cxStyle278
      Styles.Header = cxStyle279
      Styles.Inactive = cxStyle280
      Styles.Indicator = cxStyle282
      Styles.Preview = cxStyle283
      Styles.Selection = cxStyle284
      BuiltIn = True
    end
  end
  object PROC_KS_PROVERI_DEF_PARTOVI: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_KS_PROVERI_DEF_PARTOVI (?KS_ID)')
    StoredProcName = 'PROC_KS_PROVERI_DEF_PARTOVI'
    Left = 672
    Top = 456
  end
  object tblChekIn: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.id,'
      '       p.studija,'
      '       p.rab_mesto,'
      '       rmv.naziv as rab_mesto_naziv,'
      '       p.part,'
      '       kp.part as part_naziv,'
      '       p.datum,'
      '       p.vreme,'
      '       p.dobrovolec,'
      '       p.aktiven_dobrovolec,'
      '       p.tocka_br,'
      '       pt.vremenska_tocka,'
      '       (select  sum(sl.primerok_kolicina)'
      '          from ks_studija_lekovi sl'
      '          where sl.studija_id = :mas_id)as primerok_kolicina,'
      '       p.reden_broj,'
      '       p.broj_epruveta,'
      '       p.vreme_realizacija,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from ks_primeroci p'
      'inner join ks_part_tocki pt on pt.id = p.part'
      'inner join ks_partovi kp on kp.id = pt.part_id'
      
        'inner join ks_rabotno_mesto_vraboteni rmv on rmv.id = p.rab_mest' +
        'o'
      'where p.studija = :mas_id   and kp.tip = - 3'
      'order by p.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsStudija
    Left = 40
    Top = 936
    object tblChekInID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblChekInSTUDIJA: TFIBIntegerField
      DisplayLabel = #1057#1090#1091#1076#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'STUDIJA'
    end
    object tblChekInRAB_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RAB_MESTO'
    end
    object tblChekInRAB_MESTO_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RAB_MESTO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInPART: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PART'
    end
    object tblChekInPART_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090
      FieldName = 'PART_NAZIV'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblChekInVREME: TFIBTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
      DisplayFormat = 'hh:mm:ss'
    end
    object tblChekInDOBROVOLEC: TFIBStringField
      DisplayLabel = #1044#1086#1073#1088#1086#1074#1086#1083#1077#1094
      FieldName = 'DOBROVOLEC'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInAKTIVEN_DOBROVOLEC: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1076#1086#1073#1088#1086#1074#1086#1083#1077#1094
      FieldName = 'AKTIVEN_DOBROVOLEC'
    end
    object tblChekInTOCKA_BR: TFIBFloatField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1090#1086#1095#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'TOCKA_BR'
    end
    object tblChekInREDEN_BROJ: TFIBIntegerField
      DisplayLabel = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
      FieldName = 'REDEN_BROJ'
    end
    object tblChekInBROJ_EPRUVETA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1077#1087#1088#1091#1074#1077#1090#1072
      FieldName = 'BROJ_EPRUVETA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInVREME_REALIZACIJA: TFIBTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'VREME_REALIZACIJA'
      DisplayFormat = 'hh:mm:ss'
    end
    object tblChekInCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblChekInTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblChekInUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblChekInVREMENSKA_TOCKA: TFIBBCDField
      DisplayLabel = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1090#1086#1095#1082#1072
      FieldName = 'VREMENSKA_TOCKA'
      Size = 3
    end
    object tblChekInPRIMEROK_KOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082' ('#1084#1083')'
      FieldName = 'PRIMEROK_KOLICINA'
      Size = 0
    end
  end
  object dsChekIn: TDataSource
    DataSet = tblChekIn
    Left = 176
    Top = 936
  end
  object tblBarkodDobrovolci: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct proc_ks_barcode_dobrovolec.kod_studija,'
      '       proc_ks_barcode_dobrovolec.dobrovolec,'
      '       proc_ks_barcode_dobrovolec.studija_id,'
      '       proc_ks_barcode_dobrovolec.barcode,'
      '       proc_ks_barcode_dobrovolec.dobrovolec_id'
      'from proc_ks_barcode_dobrovolec(:ks_id)'
      'order by 2')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 352
    Top = 336
    object tblBarkodDobrovolciKOD_STUDIJA: TFIBStringField
      FieldName = 'KOD_STUDIJA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciDOBROVOLEC: TFIBStringField
      FieldName = 'DOBROVOLEC'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciSTUDIJA_ID: TFIBStringField
      FieldName = 'STUDIJA_ID'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciDOBROVOLEC_ID: TFIBStringField
      FieldName = 'DOBROVOLEC_ID'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsBarkodDobrovolci: TDataSource
    Left = 488
    Top = 336
  end
  object tblBarkodEpruveti: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_ks_barcode_epruveta.rab_mesto,'
      '       proc_ks_barcode_epruveta.par_tip,'
      '       proc_ks_barcode_epruveta.part_id,'
      '       proc_ks_barcode_epruveta.dobrovolec,'
      '       proc_ks_barcode_epruveta.tocka_br,'
      '       proc_ks_barcode_epruveta.broj_epruveta,'
      '       proc_ks_barcode_epruveta.part,'
      '       proc_ks_barcode_epruveta.kod_studija,'
      '       proc_ks_barcode_epruveta.studija_id,'
      '       proc_ks_barcode_epruveta.barcode,'
      '       proc_ks_barcode_epruveta.broj_epruveta_id,'
      '       proc_ks_barcode_epruveta.dobrovolec_id'
      '    from proc_ks_barcode_epruveta(:ks_id)'
      '    order by 1, 2, 3,4,5')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 352
    Top = 400
    object tblBarkodEpruvetiKOD_STUDIJA: TFIBStringField
      FieldName = 'KOD_STUDIJA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiDOBROVOLEC: TFIBStringField
      FieldName = 'DOBROVOLEC'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiSTUDIJA_ID: TFIBStringField
      FieldName = 'STUDIJA_ID'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiTOCKA_BR: TFIBFloatField
      FieldName = 'TOCKA_BR'
    end
    object tblBarkodEpruvetiBROJ_EPRUVETA: TFIBStringField
      FieldName = 'BROJ_EPRUVETA'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiBROJ_EPRUVETA_ID: TFIBStringField
      FieldName = 'BROJ_EPRUVETA_ID'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPART: TFIBStringField
      FieldName = 'PART'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiDOBROVOLEC_ID: TFIBStringField
      FieldName = 'DOBROVOLEC_ID'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsBarkodEpruveti: TDataSource
    DataSet = tblBarkodEpruveti
    Left = 488
    Top = 400
  end
  object qCountVremeRealizacija: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  count(p.id) as br'
      'from ks_primeroci p'
      
        'where p.vreme_realizacija is not null and p.studija = :studija_i' +
        'd')
    Left = 672
    Top = 536
  end
  object tblBarkodDobrovolciPecati: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.*'
      'from (select distinct proc_ks_barcode_dobrovolec.kod_studija,'
      '                      proc_ks_barcode_dobrovolec.dobrovolec,'
      '                      proc_ks_barcode_dobrovolec.studija_id,'
      '                      proc_ks_barcode_dobrovolec.barcode,'
      '                      proc_ks_barcode_dobrovolec.dobrovolec_id'
      '       from proc_ks_barcode_dobrovolec(:ks_id)'
      '       order by 2) s'
      'where s.dobrovolec >=:broj_od and s.dobrovolec<=:broj_do')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 344
    Top = 464
    object tblBarkodDobrovolciPecatiKOD_STUDIJA: TFIBStringField
      FieldName = 'KOD_STUDIJA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciPecatiDOBROVOLEC: TFIBStringField
      FieldName = 'DOBROVOLEC'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciPecatiSTUDIJA_ID: TFIBStringField
      FieldName = 'STUDIJA_ID'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciPecatiBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodDobrovolciPecatiDOBROVOLEC_ID: TFIBStringField
      FieldName = 'DOBROVOLEC_ID'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblBarkodEpruvetiPecati: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.*'
      'from (select proc_ks_barcode_epruveta.rab_mesto,'
      '       proc_ks_barcode_epruveta.par_tip,'
      '       proc_ks_barcode_epruveta.part_id,'
      '       proc_ks_barcode_epruveta.dobrovolec,'
      '       proc_ks_barcode_epruveta.tocka_br,'
      '       proc_ks_barcode_epruveta.broj_epruveta,'
      '       proc_ks_barcode_epruveta.part,'
      '       proc_ks_barcode_epruveta.kod_studija,'
      '       proc_ks_barcode_epruveta.studija_id,'
      '       proc_ks_barcode_epruveta.barcode,'
      '       proc_ks_barcode_epruveta.broj_epruveta_id,'
      '       proc_ks_barcode_epruveta.dobrovolec_id'
      '    from proc_ks_barcode_epruveta(:ks_id)'
      '    order by  1, 3,4,5) s'
      'where s.dobrovolec >= :broj_od and s.dobrovolec<=:broj_do')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 488
    Top = 464
    object tblBarkodEpruvetiPecatiRAB_MESTO: TFIBIntegerField
      FieldName = 'RAB_MESTO'
    end
    object tblBarkodEpruvetiPecatiPAR_TIP: TFIBIntegerField
      FieldName = 'PAR_TIP'
    end
    object tblBarkodEpruvetiPecatiPART_ID: TFIBIntegerField
      FieldName = 'PART_ID'
    end
    object tblBarkodEpruvetiPecatiDOBROVOLEC: TFIBStringField
      FieldName = 'DOBROVOLEC'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiTOCKA_BR: TFIBFloatField
      FieldName = 'TOCKA_BR'
    end
    object tblBarkodEpruvetiPecatiBROJ_EPRUVETA: TFIBStringField
      FieldName = 'BROJ_EPRUVETA'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiPART: TFIBStringField
      FieldName = 'PART'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiKOD_STUDIJA: TFIBStringField
      FieldName = 'KOD_STUDIJA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiSTUDIJA_ID: TFIBStringField
      FieldName = 'STUDIJA_ID'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiBROJ_EPRUVETA_ID: TFIBStringField
      FieldName = 'BROJ_EPRUVETA_ID'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBarkodEpruvetiPecatiDOBROVOLEC_ID: TFIBStringField
      FieldName = 'DOBROVOLEC_ID'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblLogs: TpFIBDataSet
    SelectSQL.Strings = (
      'select l.id,'
      '       l.log_tag,'
      '       l.korisnicko_ime,'
      '       l.opis,'
      '       l.vreme,'
      '       l.datum,'
      '       l.ts_ins,'
      '       l.log_id,'
      '       l.studija_id,'
      '       s.kod_studija,'
      '       v.ime_prezime,'
      
        '       case when l.log_tag = 111 then '#39#1044#1086#1073#1088#1086#1074#1086#1083#1077#1094#1086#1090' '#1085#1077' '#1077' '#1088#1077#1075#1080#1089#1090#1088 +
        #1080#1088#1072#1085' '#1074#1086' '#1086#1074#1072#1072' '#1089#1090#1091#1076#1080#1112#1072#39
      
        '            when l.log_tag = 222 then '#39#1044#1086#1073#1088#1086#1074#1086#1083#1077#1094#1086#1090' '#1085#1077' '#1077' '#1085#1072' '#1086#1074#1072' ' +
        #1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086#39
      
        '            when l.log_tag = 333 then '#39#1044#1086#1073#1088#1086#1074#1086#1083#1077#1094#1086#1090' '#1085#1077' '#1077' '#1074#1086' '#1076#1077#1092#1080 +
        #1085#1080#1088#1072#1085#1072#1090#1072' '#1074#1088#1077#1084#1077#1085#1089#1082#1072' '#1088#1072#1084#1082#1072#39
      
        '            when l.log_tag = 444 then '#39#1044#1086#1073#1088#1086#1074#1086#1083#1077#1094#1086#1090' '#1087#1086#1084#1080#1085#1072' '#1085#1072' '#1088#1077 +
        #1076#1086#1090#39
      
        '            when l.log_tag = 555 then '#39#1044#1086#1073#1088#1086#1074#1086#1083#1077#1094#1086#1090' '#1080' '#1077#1087#1088#1091#1074#1077#1090#1072#1090#1072 +
        ' '#1085#1077' '#1089#1077' '#1089#1086#1074#1087#1072#1107#1072#1072#1090#39
      
        '            when l.log_tag = 666 then '#39#1055#1086#1075#1088#1077#1096#1077#1085' '#1073#1072#1088#1082#1086#1076' '#1085#1072' '#1076#1086#1073#1088#1086#1074 +
        #1086#1083#1077#1094#39
      
        '            when l.log_tag = 777 then '#39#1055#1086#1075#1088#1077#1096#1077#1085' '#1073#1072#1088#1082#1086#1076' '#1085#1072' '#1077#1087#1088#1091#1074#1077 +
        #1090#1072#39
      
        '            when l.log_tag = 888 then '#39#1045#1087#1088#1091#1074#1077#1090#1072#1090#1072' '#1085#1077' '#1077' '#1076#1077#1092#1080#1085#1080#1088#1072#1085 +
        #1072' '#1085#1072' '#1086#1074#1072#1072' '#1089#1090#1091#1076#1080#1112#1072#39
      
        '            when l.log_tag = 999 then '#39#1053#1072#1112#1087#1088#1074#1086' '#1074#1085#1077#1089#1077#1090#1077' '#1090#1086#1095#1077#1085' '#1076#1086#1073 +
        #1088#1086#1074#1086#1083#1094#39
      '       end "log_tag_naziv"'
      ''
      'from ks_log l'
      'inner join ks_studija s on s.id = l.studija_id'
      'inner join ks_vraboteni v on v.korisnicko_ime = l.korisnicko_ime'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 40
    Top = 1008
    object tblLogsID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLogsLOG_TAG: TFIBIntegerField
      DisplayLabel = #1058#1040#1043
      FieldName = 'LOG_TAG'
    end
    object tblLogsKORISNICKO_IME: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1095#1082#1086' '#1080#1084#1077
      FieldName = 'KORISNICKO_IME'
      Size = 8
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLogsOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLogsVREME: TFIBStringField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLogsDATUM: TFIBStringField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLogsTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblLogsLOG_ID: TFIBIntegerField
      FieldName = 'LOG_ID'
    end
    object tblLogsSTUDIJA_ID: TFIBIntegerField
      DisplayLabel = #1057#1090#1091#1076#1080#1112#1072'-'#1096#1080#1092#1088#1072
      FieldName = 'STUDIJA_ID'
    end
    object tblLogsKOD_STUDIJA: TFIBStringField
      DisplayLabel = #1050#1086#1076' '#1085#1072' '#1089#1090#1091#1076#1080#1112#1072
      FieldName = 'KOD_STUDIJA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLogsIME_PREZIME: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'IME_PREZIME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLogslog_tag_naziv: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1075#1088#1077#1096#1082#1072
      FieldName = 'log_tag_naziv'
      Size = 49
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLogs: TDataSource
    DataSet = tblLogs
    Left = 168
    Top = 1008
  end
  object qCountVrabotenRM: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(rmv.id) br'
      'from ks_rabotno_mesto_vraboteni rmv'
      
        'where rmv.studija = :ks_id and rmv.vraboten = :vraboten and rmv.' +
        'id <> :rm_id')
    Left = 672
    Top = 608
  end
end
