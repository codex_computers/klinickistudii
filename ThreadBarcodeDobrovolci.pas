unit ThreadBarcodeDobrovolci;

interface

uses
   StrUtils,Dialogs,Windows,WinINet, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
   Buttons, ExtCtrls,DateUtils,ComCtrls, pFIBDataSet,FIBDataSet,pFibScripter,FIBDatabase,
   pFIBDatabase,ActiveX,frxClass,  frxDBSet, frxDesgn, frxBarcode;

type
  TBarcodeThread = class(TThread)
  private
    { Private declarations }
  protected
    procedure Execute; override;
  public
     start,stop,printer:string;ks_id, broj, tag_param:integer;
  end;
  Procedure PrintBarcodeDobrovolec({startb,stopb,}printerb:string; br, ks_id, tag_param:integer);
implementation


uses dmResources, dmUnit;

{ TBarcodeThread }

Procedure PrintBarcodeDobrovolec(printerb:string; br, ks_id, tag_param:integer);

var BarcodeThread: TBarcodeThread;

begin

  BarcodeThread := TBarcodeThread.Create(true);
  BarcodeThread.FreeOnTerminate := true;
  BarcodeThread.broj := br;
  BarcodeThread.printer := printerb;
  BarcodeThread.ks_id := ks_id;
  BarcodeThread.tag_param :=tag_param;
  BarcodeThread.Execute;
end;

procedure TBarcodeThread.Execute;
  var  r, j:integer;
    kod:string;
    BarCode: TfrxBarCodeObject;
begin
  CoInitialize(nil);
  if (tag_param =1) then
   begin
     dm.tblBarkodDobrovolci.First;
     r:=0;
     while (not dm.tblBarkodDobrovolci.Eof) do
       begin
        r:=r+1;
        if (not dm.tblBarkodDobrovolci.FieldByName('BARCODE').IsNull) then
           begin
               dmRes.Spremi('KS',7);
               dmRes.frxReport1.Variables.AddVariable('VAR', 'barcode', QuotedStr(dm.tblBarkodDobrovolciBARCODE.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'studija', QuotedStr(dm.tblBarkodDobrovolciKOD_STUDIJA.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'dobrovolec', QuotedStr(dm.tblBarkodDobrovolciDOBROVOLEC_ID.Value));
               dmRes.frxreport1.Variables['no']:=r;

               dmRes.frxReport1.PrintOptions.ShowDialog:=False;
               dmRes.frxReport1.PrintOptions.Printer:=Printer;
               dmRes.frxreport1.PrepareReport(true);
             //  dmRes.frxreport1.ShowReport();

               dmRes.frxreport1.Print;
           end;
        dm.tblBarkodDobrovolci.Next;
       end;
   end
  else if (tag_param =2) then
   begin
     dm.tblBarkodDobrovolciPecati.First;
     r:=0;
     while (not dm.tblBarkodDobrovolciPecati.Eof) do
       begin
        r:=r+1;
        if (not dm.tblBarkodDobrovolciPecati.FieldByName('BARCODE').IsNull) then
           begin
               dmRes.Spremi('KS',7);
               dmRes.frxReport1.Variables.AddVariable('VAR', 'barcode', QuotedStr(dm.tblBarkodDobrovolciPecatiBARCODE.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'studija', QuotedStr(dm.tblBarkodDobrovolciPecatiKOD_STUDIJA.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'dobrovolec', QuotedStr(dm.tblBarkodDobrovolciPecatiDOBROVOLEC_ID.Value));
               dmRes.frxreport1.Variables['no']:=r;

               dmRes.frxReport1.PrintOptions.ShowDialog:=False;
               dmRes.frxReport1.PrintOptions.Printer:=Printer;
               dmRes.frxreport1.PrepareReport(true);
               dmRes.frxreport1.Print;
               //dmRes.frxreport1.ShowReport();

           end;
        dm.tblBarkodDobrovolciPecati.Next;
       end;
   end;
  CoUninitialize();
end;

end.
