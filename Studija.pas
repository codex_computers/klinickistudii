unit Studija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, dxBarBuiltInMenu, cxNavigator, dxCore, cxDateUtils,
  System.Actions, cxImageComboBox, cxTimeEdit, cxSplitter, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmStudija = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    Panel1: TPanel;
    cxTabSheet5: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxTabSheet7: TcxTabSheet;
    cxGrid5: TcxGrid;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid6: TcxGrid;
    cxGrid6DBTableView1: TcxGridDBTableView;
    cxGrid6Level1: TcxGridLevel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1KOD_STUDIJA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1NARACATEL: TcxGridDBColumn;
    cxGrid1DBTableView1NARACATELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1BR_RAB_MESTA: TcxGridDBColumn;
    cxGrid1DBTableView1BR_UCESNICI: TcxGridDBColumn;
    cxGrid1DBTableView1PARTOVI: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dPanel: TPanel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    NARACATEL: TcxDBLookupComboBox;
    KOD_STUDIJA: TcxDBTextEdit;
    OPIS: TcxDBMemo;
    DATUM: TcxDBDateEdit;
    cxGroupBox2: TcxGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    BR_RAB_MESTA: TcxDBTextEdit;
    BR_UCESNICI: TcxDBTextEdit;
    PARTOVI: TcxDBTextEdit;
    RASTOJANIE: TcxDBTextEdit;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    aGenerirajRabMestaUcenici: TAction;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1STUDIJA: TcxGridDBColumn;
    cxGrid2DBTableView1VRABOTEN: TcxGridDBColumn;
    cxGrid2DBTableView1IME_PREZIME: TcxGridDBColumn;
    cxGrid2DBTableView1BR_UCESNICI: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1STUDIJA_ID: TcxGridDBColumn;
    cxGrid3DBTableView1LEK_ID: TcxGridDBColumn;
    cxGrid3DBTableView1LEK_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid4DBTableView1ID: TcxGridDBColumn;
    cxGrid4DBTableView1STUDIJA: TcxGridDBColumn;
    cxGrid4DBTableView1POCETEN_DATUM: TcxGridDBColumn;
    cxGrid4DBTableView1BROJ_TOCKI: TcxGridDBColumn;
    cxGrid4DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid4DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid4DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid4DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid4DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid4DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid4DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid4DBTableView1PART: TcxGridDBColumn;
    cxGrid5DBTableView1ID: TcxGridDBColumn;
    cxGrid5DBTableView1PART_ID: TcxGridDBColumn;
    cxGrid5DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn;
    cxGrid5DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid5DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid5DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid5DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid5DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid5DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid5DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid4DBTableView1POCETNO_VREME: TcxGridDBColumn;
    aGenerirajPartTocki: TAction;
    aGenerirajPrimeroci: TAction;
    cxGrid6DBTableView1ID: TcxGridDBColumn;
    cxGrid6DBTableView1STUDIJA: TcxGridDBColumn;
    cxGrid6DBTableView1RAB_MESTO: TcxGridDBColumn;
    cxGrid6DBTableView1RAB_MESTO_NAZIV: TcxGridDBColumn;
    cxGrid6DBTableView1PART: TcxGridDBColumn;
    cxGrid6DBTableView1PART_NAZIV: TcxGridDBColumn;
    cxGrid6DBTableView1DATUM: TcxGridDBColumn;
    cxGrid6DBTableView1VREME: TcxGridDBColumn;
    cxGrid6DBTableView1BROJ_EPRUVETA: TcxGridDBColumn;
    cxGrid6DBTableView1VREME_REALIZACIJA: TcxGridDBColumn;
    cxGrid6DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid6DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid6DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid6DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid6DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid6DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid6DBTableView1USR_UPD: TcxGridDBColumn;
    Panel2: TPanel;
    cxButton2: TcxButton;
    abrisiPrimeroci: TAction;
    aListaPrimerociSite: TAction;
    PopupMenu2: TPopupMenu;
    aDMesecenIzvestaj1: TMenuItem;
    N2: TMenuItem;
    aDizajnReport: TAction;
    aDListaPrimeroci: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aFormularPodgotovkaEpruvetaPufer: TAction;
    aDFormularPodgotovkaEpruvetaPufer: TAction;
    Panel3: TPanel;
    cxButton1: TcxButton;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGridPopupMenu4: TcxGridPopupMenu;
    cxGridPopupMenu5: TcxGridPopupMenu;
    cxGridPopupMenu6: TcxGridPopupMenu;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    dxBarLargeButton20: TdxBarLargeButton;
    aZacuvajExcelPrimeroci: TAction;
    aPecatiTabelaPrimeroci: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarButton1: TdxBarButton;
    cxSplitter1: TcxSplitter;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    aListaPrimerociSitePoPartRabotnoMesto: TAction;
    dxBarButton3: TdxBarButton;
    PanelPecatiLista: TPanel;
    cxGroupBox3: TcxGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    cbPart: TcxLookupComboBox;
    cbRabMesto: TcxLookupComboBox;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    aOtkaziPecatenjeLista: TAction;
    aPecatenjeLista: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    Label11: TLabel;
    cbVremenskaTocka: TcxLookupComboBox;
    Label12: TLabel;
    aFormularBrzoSmrznuvanje: TAction;
    aDFormularBrzoSmrznuvanje: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    N3: TMenuItem;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton23: TdxBarLargeButton;
    aFormularVremenskaListaCentrifugiranje: TAction;
    aDFormularVremenskaListaCentrifugiranje: TAction;
    N4: TMenuItem;
    aFormularVremenskaListaSkladiranje: TAction;
    aDFormularVremenskaListaSkladiranje: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    dxBarButton13: TdxBarButton;
    N5: TMenuItem;
    cxGrid5DBTableView1VREMENSKA_TOCKA: TcxGridDBColumn;
    cxGrid6DBTableView1VREMENSKA_TOCKA: TcxGridDBColumn;
    cxGrid6DBTableView1AKTIVEN_DOBROVOLEC: TcxGridDBColumn;
    dxBarLargeButton25: TdxBarLargeButton;
    aPecatiSiteFormulari: TAction;
    cxGrid1DBTableView1BR_REZERVI: TcxGridDBColumn;
    cxGrid2DBTableView1BR_REZERVI: TcxGridDBColumn;
    cxGrid1DBTableView1RASTOJANIE: TcxGridDBColumn;
    cxGrid4DBTableView1TIP: TcxGridDBColumn;
    cxGrid6DBTableView1DOBROVOLEC: TcxGridDBColumn;
    cxGrid1DBTableView1CHEK_IN: TcxGridDBColumn;
    cxTabSheet6: TcxTabSheet;
    cxGrid7: TcxGrid;
    cxGrid7DBTableView1: TcxGridDBTableView;
    cxGrid7Level1: TcxGridLevel;
    cxGrid7DBTableView1ID: TcxGridDBColumn;
    cxGrid7DBTableView1STUDIJA: TcxGridDBColumn;
    cxGrid7DBTableView1RAB_MESTO: TcxGridDBColumn;
    cxGrid7DBTableView1RAB_MESTO_NAZIV: TcxGridDBColumn;
    cxGrid7DBTableView1PART: TcxGridDBColumn;
    cxGrid7DBTableView1PART_NAZIV: TcxGridDBColumn;
    cxGrid7DBTableView1DATUM: TcxGridDBColumn;
    cxGrid7DBTableView1VREME: TcxGridDBColumn;
    cxGrid7DBTableView1DOBROVOLEC: TcxGridDBColumn;
    cxGrid7DBTableView1AKTIVEN_DOBROVOLEC: TcxGridDBColumn;
    cxGrid7DBTableView1TOCKA_BR: TcxGridDBColumn;
    cxGrid7DBTableView1REDEN_BROJ: TcxGridDBColumn;
    cxGrid7DBTableView1BROJ_EPRUVETA: TcxGridDBColumn;
    cxGrid7DBTableView1VREME_REALIZACIJA: TcxGridDBColumn;
    cxGrid7DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid7DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid7DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid7DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid7DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid7DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid7DBTableView1USR_UPD: TcxGridDBColumn;
    cxGridPopupMenu7: TcxGridPopupMenu;
    cxGroupBox4: TcxGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    cxDBCheckBoxCHEK_IN: TcxDBCheckBox;
    CHEK_IN_RASTOJANIE: TcxDBTextEdit;
    cxGroupBox5: TcxGroupBox;
    Label13: TLabel;
    BR_REZERVI: TcxDBTextEdit;
    cxDBCheckBoxREZERVI_NULTA_TOCKA: TcxDBCheckBox;
    cxGrid1DBTableView1REZERVI_NULTA_TOCKA: TcxGridDBColumn;
    cxGrid1DBTableView1CHEK_IN_RASTOJANIE: TcxGridDBColumn;
    aPChekInLista: TAction;
    aDChekInLista: TAction;
    dxBarLargeButton26: TdxBarLargeButton;
    N6: TMenuItem;
    dxBarLargeButton27: TdxBarLargeButton;
    aBarkodEpruveti: TAction;
    dxBarSubItem4: TdxBarSubItem;
    aBarkodDobrovolci: TAction;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton17: TdxBarButton;
    aDBarkodDobrovolci: TAction;
    N7: TMenuItem;
    aDBarkodEpruveti: TAction;
    N8: TMenuItem;
    Label16: TLabel;
    MINUTI_PLUS_MINUS: TcxDBTextEdit;
    Label17: TLabel;
    cxGrid1DBTableView1MINUTI_PLUS_MINUS: TcxGridDBColumn;
    dxBarLargeButton28: TdxBarLargeButton;
    aOsveziListaPrimeroci: TAction;
    dxBarLargeButton29: TdxBarLargeButton;
    aPromeniStatusStudija: TAction;
    PanelPecatiBarkodDobrovolci: TPanel;
    cxGroupBoxBarkode: TcxGroupBox;
    cxGridPopupMenu8: TcxGridPopupMenu;
    Label18: TLabel;
    Label19: TLabel;
    BROJ_OD: TcxTextEdit;
    BROJ_DO: TcxTextEdit;
    btnPecatiBD: TcxButton;
    btnOdtaziBD: TcxButton;
    aOdkaziBD: TAction;
    aPecatiBD: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    aPVremeRealizacija: TAction;
    aDVremeRealizacija: TAction;
    Bloodsamplingtimerecord1: TMenuItem;
    cxGrid7DBTableView1VREMENSKA_TOCKA: TcxGridDBColumn;
    cxGrid7DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn;
    cxGrid6DBTableView1PRIMEROK_KOLICINA: TcxGridDBColumn;
    cxGrid5DBTableView1BR_EPRUVETI: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure GenerirajRabMestaUcenici(state: Integer);
    procedure GenerirajPartovi(state: Integer);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid3DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid4DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aGenerirajPartTockiExecute(Sender: TObject);
    procedure aGenerirajPrimerociExecute(Sender: TObject);
    procedure cxGrid6DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid5DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure abrisiPrimerociExecute(Sender: TObject);
    procedure aListaPrimerociSiteExecute(Sender: TObject);
    procedure aDizajnReportExecute(Sender: TObject);
    procedure aDListaPrimerociExecute(Sender: TObject);
    procedure aFormularPodgotovkaEpruvetaPuferExecute(Sender: TObject);
    procedure aDFormularPodgotovkaEpruvetaPuferExecute(Sender: TObject);
    procedure cxGrid2DBTableView1IME_PREZIMEPropertiesCloseUp(Sender: TObject);
    procedure aZacuvajExcelPrimerociExecute(Sender: TObject);
    procedure aPecatiTabelaPrimerociExecute(Sender: TObject);
    procedure aListaPrimerociSitePoPartRabotnoMestoExecute(Sender: TObject);
    procedure aPecatenjeListaExecute(Sender: TObject);
    procedure cbPartPropertiesChange(Sender: TObject);
    procedure cxGrid2DBTableView1StylesGetFooterStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aFormularBrzoSmrznuvanjeExecute(Sender: TObject);
    procedure aDFormularBrzoSmrznuvanjeExecute(Sender: TObject);
    procedure aFormularVremenskaListaCentrifugiranjeExecute(Sender: TObject);
    procedure aDFormularVremenskaListaCentrifugiranjeExecute(Sender: TObject);
    procedure aFormularVremenskaListaSkladiranjeExecute(Sender: TObject);
    procedure aDFormularVremenskaListaSkladiranjeExecute(Sender: TObject);
    procedure cxGrid6DBTableView1AKTIVEN_DOBROVOLECPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGrid6DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aPecatiSiteFormulariExecute(Sender: TObject);
    procedure cxGrid4DBTableView1POCETEN_DATUMStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid4DBTableView1POCETNO_VREMEStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid4DBTableView1PARTStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid4DBTableView1BROJ_TOCKIStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aPChekInListaExecute(Sender: TObject);
    procedure aDChekInListaExecute(Sender: TObject);
    procedure cxDBCheckBoxCHEK_INPropertiesChange(Sender: TObject);
    procedure aBarkodEpruvetiExecute(Sender: TObject);
    procedure aBarkodDobrovolciExecute(Sender: TObject);
    procedure aDBarkodDobrovolciExecute(Sender: TObject);
    procedure aDBarkodEpruvetiExecute(Sender: TObject);
    procedure aOsveziListaPrimerociExecute(Sender: TObject);
    procedure aPromeniStatusStudijaExecute(Sender: TObject);
    procedure aOdkaziBDExecute(Sender: TObject);
    procedure aPecatiBDExecute(Sender: TObject);
    procedure aPVremeRealizacijaExecute(Sender: TObject);
    procedure aDVremeRealizacijaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
    function status : integer;
    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmStudija: TfrmStudija;
  rData : TRepositoryData;
  rab_mesta_1, br_ucesnici_1,  br_part, br_primeroci, tag_izvestaj, CHEK_IN_v,BR_REZERVI_v, REZERVI_NULTA_TOCKA_v:Integer;
  rastojanie_1, CHEK_IN_RASTOJANIE_v:Double;
implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, Naracateli,
  Lekovi, ThreadBarcodeDobrovolci, ThreadBarcodeEpruveti;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmStudija.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmStudija.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    cxPageControl1.ActivePage:=cxTabSheet2;
    dPanel.Enabled:=True;
    KOD_STUDIJA.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmStudija.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
    if dm.tblStudijaSTATUS.Value = 1 then
      begin
        dm.qCountVremeRealizacija.Close;
        dm.qCountVremeRealizacija.ParamByName('studija_id').Value:=dm.tblStudijaID.Value;
        dm.qCountVremeRealizacija.ExecQuery;
        if dm.qCountVremeRealizacija.FldByName['br'].Value = 0 then
          begin
            dm.qCountPrimeroci.Close;
            dm.qCountPrimeroci.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
            dm.qCountPrimeroci.ExecQuery;
            if dm.qCountPrimeroci.FldByName['br'].Value > 0 then
               begin
                  frmDaNe := TfrmDaNe.Create(self, '��������� �� �����', '���� ��������� ������ �� �� ��������� �������, ���������� � ����� �� ���������?', 1);
                  if (frmDaNe.ShowModal = mrYes) then
                    begin
                      br_primeroci:=1;
                      rab_mesta_1:=dm.tblStudijaBR_RAB_MESTA.Value;
                      br_ucesnici_1:=dm.tblStudijaBR_UCESNICI.Value;
                      rastojanie_1:=dm.tblStudijaRASTOJANIE.Value;
                      br_part:=dm.tblStudijaPARTOVI.Value;
                      CHEK_IN_RASTOJANIE_v:=dm.tblStudijaCHEK_IN_RASTOJANIE.Value;
                      CHEK_IN_v:=dm.tblStudijaCHEK_IN.Value;
                      BR_REZERVI_v:=dm.tblStudijaBR_REZERVI.Value;
                      REZERVI_NULTA_TOCKA_v:=dm.tblStudijaREZERVI_NULTA_TOCKA.Value;
                      cxPageControl1.ActivePage:=cxTabSheet2;
                      dPanel.Enabled:=True;
                      KOD_STUDIJA.SetFocus;
                      cxGrid1DBTableView1.DataController.DataSet.Edit;
                    end
                  else Abort;
               end
            else
                begin
                  rab_mesta_1:=dm.tblStudijaBR_RAB_MESTA.Value;
                  br_ucesnici_1:=dm.tblStudijaBR_UCESNICI.Value;
                  rastojanie_1:=dm.tblStudijaRASTOJANIE.Value;
                  br_part:=dm.tblStudijaPARTOVI.Value;
                  cxPageControl1.ActivePage:=cxTabSheet2;
                  dPanel.Enabled:=True;
                  KOD_STUDIJA.SetFocus;
                  cxGrid1DBTableView1.DataController.DataSet.Edit;
                end
          end
        else ShowMessage('�� � ��������� ��������� �� ��������. ��������� � ���������� !!!')
      end
    else  ShowMessage('�� � ��������� ��������� �� ��������. �������� � �������� !!!')
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmStudija.aBarkodDobrovolciExecute(Sender: TObject);
begin
    PanelPecatiBarkodDobrovolci.Visible:=True;
    BROJ_OD.Clear;
    BROJ_DO.Clear;
    BROJ_OD.SetFocus;
    PanelPecatiBarkodDobrovolci.Top:=132;
    PanelPecatiBarkodDobrovolci.Left:=624;
    PanelPecatiBarkodDobrovolci.Tag:=1;
    cxGroupBoxBarkode.Caption:='������ ����������';
end;

procedure TfrmStudija.aBarkodEpruvetiExecute(Sender: TObject);
begin
    PanelPecatiBarkodDobrovolci.Visible:=True;
    BROJ_OD.Clear;
    BROJ_DO.Clear;
    BROJ_OD.SetFocus;
    PanelPecatiBarkodDobrovolci.Top:=132;
    PanelPecatiBarkodDobrovolci.Left:=624;
    PanelPecatiBarkodDobrovolci.Tag:=2;
    cxGroupBoxBarkode.Caption:='������ ��������';
end;

procedure TfrmStudija.aDBarkodDobrovolciExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',7);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDBarkodEpruvetiExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',8);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
   if dm.tblStudijaSTATUS.Value = 1 then
     begin
      dm.qCountPrimeroci.Close;
      dm.qCountPrimeroci.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
      dm.qCountPrimeroci.ExecQuery;
      if dm.qCountPrimeroci.FldByName['br'].Value > 0 then
          frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ������� �������, ���������� � ����� �� ���������?', 1)
      else
        frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
      if (frmDaNe.ShowModal = mrYes) then
        cxGrid1DBTableView1.DataController.DataSet.Delete()
      else Abort;
     end
   else ShowMessage('�� � ��������� ������ �� ��������. �������� � �������� !!!')
end;

procedure TfrmStudija.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmStudija.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmStudija.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmStudija.aListaPrimerociSiteExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('KS',1);
        dmKon.tblSqlReport.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        dmKon.tblSqlReport.ParamByName('part_id').Value:='%';
        dmKon.tblSqlReport.ParamByName('rm_id').Value:='%';
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmStudija.aListaPrimerociSitePoPartRabotnoMestoExecute(
  Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=1;

      PanelPecatiLista.Visible:= True;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmStudija.aPecatiBDExecute(Sender: TObject);
var   dobrovolec_od, dobrovolec_do, epruveta_od, epruveta_do :string;
begin
if PanelPecatiBarkodDobrovolci.Tag = 1 then
  begin
   if (BROJ_OD.Text<>'')and (BROJ_DO.Text <>'') then
      begin
        dobrovolec_od:= BROJ_OD.Text;
        while (length(dobrovolec_od) < 4 )do
            begin
               dobrovolec_od:='0'+dobrovolec_od;
            end;
        dobrovolec_do:= BROJ_DO.Text;
        while (length(dobrovolec_do) < 4 )do
            begin
               dobrovolec_do:='0'+dobrovolec_do;
            end;
        dm.tblBarkodDobrovolciPecati.Close;
        dm.tblBarkodDobrovolciPecati.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        dm.tblBarkodDobrovolciPecati.ParamByName('broj_od').Value:=dobrovolec_od;
        dm.tblBarkodDobrovolciPecati.ParamByName('broj_do').Value:=dobrovolec_do;
        dm.tblBarkodDobrovolciPecati.Open;
        if( not dm.tblBarkodDobrovolciPecati.IsEmpty) then
           PrintBarcodeDobrovolec(dm.barcodePrinter, dm.brLabeliStudija, dm.tblStudijaID.Value, 2)  ;
      end
   else
      begin
         dm.tblBarkodDobrovolci.Close;
         dm.tblBarkodDobrovolci.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
         dm.tblBarkodDobrovolci.Open;
         if( not dm.tblBarkodDobrovolci.IsEmpty) then
           PrintBarcodeDobrovolec(dm.barcodePrinter, dm.brLabeliStudija, dm.tblStudijaID.Value, 1)  ;
      end;
  end
else if PanelPecatiBarkodDobrovolci.Tag = 2 then
  begin
   if (BROJ_OD.Text<>'')and (BROJ_DO.Text <>'') then
      begin
        epruveta_od:= BROJ_OD.Text;
        while (length(epruveta_od) < 4 )do
            begin
               epruveta_od:='0'+epruveta_od;
            end;
        epruveta_do:= BROJ_DO.Text;
        while (length(epruveta_do) < 4 )do
            begin
               epruveta_do:='0'+epruveta_do;
            end;
        dm.tblBarkodEpruvetiPecati.Close;
        dm.tblBarkodEpruvetiPecati.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        dm.tblBarkodEpruvetiPecati.ParamByName('broj_od').Value:=epruveta_od;
        dm.tblBarkodEpruvetiPecati.ParamByName('broj_do').Value:=epruveta_do;
        dm.tblBarkodEpruvetiPecati.Open;
        if( not dm.tblBarkodEpruvetiPecati.IsEmpty) then
           PrintBarcodeEpruveti(dm.barcodePrinter, dm.brLabeliStudija, dm.tblStudijaID.Value, 2)  ;
      end
   else
      begin
        dm.tblBarkodEpruveti.Close;
        dm.tblBarkodEpruveti.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        dm.tblBarkodEpruveti.Open;

        if( not dm.tblBarkodEpruveti.IsEmpty) then
          PrintBarcodeEpruveti(dm.barcodePrinter, dm.brLabeliStudija, dm.tblStudijaID.Value,1)  ;

      end;

  end;

end;

procedure TfrmStudija.aPecatiSiteFormulariExecute(Sender: TObject);
begin
//
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=-1;

      PanelPecatiLista.Visible:= True;
 end;
procedure TfrmStudija.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmStudija.aZacuvajExcelExecute(Sender: TObject);
begin
 zacuvajVoExcel(cxGrid1, '�������� ������');
end;

procedure TfrmStudija.aZacuvajExcelPrimerociExecute(Sender: TObject);
begin
 zacuvajVoExcel(cxGrid6, '���������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmStudija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;

          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
         VK_INSERT:
                begin
                  frmNaracateli:=TfrmNaracateli.Create(self,false);
                  frmNaracateli.ShowModal;
                  if (frmNaracateli.ModalResult = mrOK) then
                    begin
                      dm.tblStudijaNARACATEL.Value := StrToInt(frmNaracateli.GetSifra(0));
                    end;
                  frmNaracateli.Free;
                end;

    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmStudija.cxDBCheckBoxCHEK_INPropertiesChange(Sender: TObject);
begin
    if dm.tblStudijaCHEK_IN.Value = 1 then
       CHEK_IN_RASTOJANIE.Tag:=1
    else
       CHEK_IN_RASTOJANIE.Tag:=0;
end;

procedure TfrmStudija.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
    if((Sender = BROJ_OD) or (Sender = BROJ_DO)or (Sender = KOD_STUDIJA))then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmStudija.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if((Sender = BROJ_OD) or (Sender = BROJ_DO)or (Sender = KOD_STUDIJA))then
            ActivateKeyboardLayout($042F042F, KLF_REORDER);
       end;
end;

procedure TfrmStudija.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
    if status = 1 then
       begin
         cxGrid2DBTableView1.OptionsData.Editing:=True;
         cxGrid3DBTableView1.OptionsData.Editing:=True;
         cxGrid4DBTableView1.OptionsData.Editing:=True;
         cxGrid5DBTableView1.OptionsData.Editing:=True;
         cxGrid6DBTableView1.OptionsData.Editing:=True;
       end
    else 
       begin
         cxGrid2DBTableView1.OptionsData.Editing:=False;
         cxGrid3DBTableView1.OptionsData.Editing:=False;
         cxGrid4DBTableView1.OptionsData.Editing:=False;
         cxGrid5DBTableView1.OptionsData.Editing:=False;
         cxGrid6DBTableView1.OptionsData.Editing:=True;
       end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmStudija.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmStudija.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmStudija.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmStudija.prefrli;
begin
end;

procedure TfrmStudija.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmStudija.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmStudija.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);

    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

    cxPageControl1.ActivePage:=cxTabSheet1;
    cxPageControl2.ActivePage:=cxTabSheet3;
    PanelPecatiLista.Top:=245;
    PanelPecatiLista.Left:=396;
end;
//------------------------------------------------------------------------------

procedure TfrmStudija.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmStudija.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmStudija.cxGrid2DBTableView1IME_PREZIMEPropertiesCloseUp(
  Sender: TObject);
begin
 if status = 1 then
  begin
   if (cxGrid2DBTableView1IME_PREZIME.EditValue <> null ) then
     begin
        dm.qCountVrabotenRM.Close;
        dm.qCountVrabotenRM.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        dm.qCountVrabotenRM.ParamByName('rm_id').Value:=dm.tblRabMestoVrabotenUcesniciID.Value;
        dm.qCountVrabotenRM.ParamByName('vraboten').Value:=cxGrid2DBTableView1IME_PREZIME.EditValue;
        dm.qCountVrabotenRM.ExecQuery;
        if dm.qCountVrabotenRM.FldByName['br'].Value = 0 then
           begin
            dm.tblRabMestoVrabotenUcesnici.Edit;
            dm.tblRabMestoVrabotenUcesniciIME_PREZIME.Value:=cxGrid2DBTableView1IME_PREZIME.EditValue;
            dm.tblRabMestoVrabotenUcesnici.Post;
           end
        else
           begin
             dm.tblRabMestoVrabotenUcesnici.Edit;
             cxGrid2DBTableView1IME_PREZIME.EditValue:=null;
             dm.tblRabMestoVrabotenUcesnici.Post;
             ShowMessage('����������� ��� � ����������� �� ������� ����� !!!');
           end;
     end
   else
     begin
        dm.tblRabMestoVrabotenUcesnici.Edit;
        dm.tblRabMestoVrabotenUcesniciIME_PREZIME.Value:='';
        dm.tblRabMestoVrabotenUcesnici.Post;
     end
  end
 else ShowMessage('�� � ��������� ��������� �� ����������. �������� � �������� !!!');
end;

function TfrmStudija.status : integer;
begin
  if dm.tblStudijaSTATUS.Value = 1 then
    Result:= 1
  else
    Result:=0;
end;

procedure TfrmStudija.cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
        VK_RETURN:
        begin
          cxGrid2DBTableView1.DataController.GotoNext;
        end;
      end;
end;

procedure TfrmStudija.cxGrid2DBTableView1StylesGetFooterStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin

    if  dm.tblStudijaBR_UCESNICI.Value = cxGrid2DBTableView1.DataController.Summary.FooterSummaryValues[6] then
        begin
         cxGrid2DBTableView1.Columns[5].Styles.Footer:=dm.dGreen;
        end
     else
         cxGrid2DBTableView1.Columns[5].Styles.Footer:=dm.dRed;

     if  dm.tblStudijaBR_REZERVI.Value = cxGrid2DBTableView1.DataController.Summary.FooterSummaryValues[4] then
        begin
         cxGrid2DBTableView1.Columns[6].Styles.Footer:=dm.dGreen;
        end
     else
         cxGrid2DBTableView1.Columns[6].Styles.Footer:=dm.dRed;
end;

procedure TfrmStudija.cxGrid3DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
        VK_RETURN:
        begin
          cxGrid3DBTableView1.DataController.GotoNext;
        end;
      end;
end;

procedure TfrmStudija.cxGrid4DBTableView1BROJ_TOCKIStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = 1) then
      begin
        AStyle := dmRes.cxStyle31;
      end
     else
       begin
        AStyle := dmRes.cxStyle108;
       end;
end;

procedure TfrmStudija.cxGrid4DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
        VK_RETURN:
        begin
          cxGrid4DBTableView1.DataController.GotoNext;
        end;
      end;
end;

procedure TfrmStudija.cxGrid4DBTableView1PARTStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
       if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -1) then
         AStyle := dmRes.cxStyle65;
       if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -2) then
         AStyle := dmRes.cxStyle65;
       if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -3) then
         AStyle :=dmRes.cxStyle65;
       if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = 1) then
         AStyle := dmRes.cxStyle31;
end;

procedure TfrmStudija.cxGrid4DBTableView1POCETEN_DATUMStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -1) then
       AStyle := dmRes.cxStyle65;
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -2) then
       AStyle := dmRes.cxStyle65;
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -3) then
         AStyle :=dmRes.cxStyle65;
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = 1) then
       begin
         AStyle := dmRes.cxStyle108;
        end;

end;

procedure TfrmStudija.cxGrid4DBTableView1POCETNO_VREMEStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -1) then
        AStyle := dmRes.cxStyle65;
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -2) then
        AStyle := dmRes.cxStyle65;
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = -3) then
         AStyle :=dmRes.cxStyle65;
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid4DBTableView1TIP.Index] = 1) then
        begin
          AStyle := dmRes.cxStyle108;
        end;
end;

procedure TfrmStudija.cxGrid5DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
        VK_RETURN:
        begin
          cxGrid5DBTableView1.DataController.GotoNext;
        end;
      end;
end;

procedure TfrmStudija.cxGrid6DBTableView1AKTIVEN_DOBROVOLECPropertiesEditValueChanged(
  Sender: TObject);
var pom:Integer;
begin
     pom:=cxGrid6DBTableView1.Controller.FocusedRowIndex;
     dm.tblPrimeroci.Edit;
     dm.tblPrimeroci.Post;
     dm.tblPrimeroci.FullRefresh;
     CxGrid6DBTableView1.Controller.FocusRecord(pom, true)
end;

procedure TfrmStudija.cxGrid6DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
        VK_RETURN:
        begin
          cxGrid6DBTableView1.DataController.GotoNext;
        end;
      end;
end;

procedure TfrmStudija.cxGrid6DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
       if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid6DBTableView1AKTIVEN_DOBROVOLEC.Index] = 1) then
         AStyle := dmRes.MoneyGreen
       else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid6DBTableView1AKTIVEN_DOBROVOLEC.Index] = 0) then
         AStyle := dmRes.RedLight;
end;

//  ����� �� �����
procedure TfrmStudija.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  brp:Integer;
begin
 // ZapisiButton.SetFocus;
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        if st in [dsEdit] then
           begin
             cxGrid1DBTableView1.DataController.DataSet.Post;
             dm.qCountPrimeroci.Close;
             dm.qCountPrimeroci.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
             dm.qCountPrimeroci.ExecQuery;
             brp:=dm.qCountPrimeroci.FldByName['br'].Value;
             if (rab_mesta_1 <> dm.tblStudijaBR_RAB_MESTA.Value) then
               begin
                 GenerirajRabMestaUcenici(2);
                 if brp > 0  then ShowMessage('���������� � ���� ����������� �� ������� ����� � ����������. ��������� � ������� �� ��������� !!!')
                 else ShowMessage('���������� � ���� ����������� �� ������� ����� � ����������. �������� � �� �� ������������ ����������� !!!');
               end;
             if ((rab_mesta_1 = dm.tblStudijaBR_RAB_MESTA.Value) and (br_ucesnici_1 <>dm.tblStudijaBR_UCESNICI.Value)) then
               begin
                 GenerirajRabMestaUcenici(3);
                 if brp > 0 then ShowMessage('���������� � ���� ����������� �� ����������. ��������� � ������� �� ���������!!!')
                 else ShowMessage('���������� � ���� ����������� �� ����������!!!');
               end;
             if (br_part <> dm.tblStudijaPARTOVI.Value) then
               begin
                 GenerirajPartovi(2);
                 if brp>0 then ShowMessage('���������� �� ���� �������. �������� � �� �� ���������� ������� ��������. ��������� � ������� �� ��������� !!!')
                 else ShowMessage('���������� �� ���� �������. �������� � �� �� ���������� ������� �������� !!!');
               end;
             if ((rastojanie_1 <> dm.tblStudijaRASTOJANIE.Value)or (CHEK_IN_RASTOJANIE_v<>dm.tblStudijaCHEK_IN_RASTOJANIE.Value)
                  or (CHEK_IN_v <>dm.tblStudijaCHEK_IN.Value) or (BR_REZERVI_v <>dm.tblStudijaBR_REZERVI.Value)
                  or (REZERVI_NULTA_TOCKA_v <> dm.tblStudijaREZERVI_NULTA_TOCKA.Value)) then
               begin
                  if brp > 0 then
                     begin
                       aGenerirajPrimeroci.Execute();
                     end;
               end;

           end
        else if st in [dsInsert] then
           begin
             cxGrid1DBTableView1.DataController.DataSet.Post;
             GenerirajRabMestaUcenici(1);
             GenerirajPartovi(1);
           end;

        dPanel.Enabled:=false;
        cxPageControl1.ActivePage:=cxTabSheet1;
        cxGrid1.SetFocus;

    end;
  end;
end;

procedure TfrmStudija.Button1Click(Sender: TObject);
begin
if status = 1 then
 begin
    frmLekovi:=TfrmLekovi.Create(self,false);
    frmLekovi.ShowModal;
    frmLekovi.Free;
    dm.tblStudijaLekovi.FullRefresh;
 end
else ShowMessage('�� � ��������� ��������� �� ����������. �������� � �������� !!!');
end;

procedure TfrmStudija.Button2Click(Sender: TObject);
begin
if status = 1 then
  begin
   dm.insert6(dm.PROC_KS_STUDIJA_LEKOVI,'KS_ID', 'LEK_ID', 'TAG', null,null, null,dm.tblStudijaLekoviSTUDIJA_ID.Value, dm.tblStudijaLekoviLEK_ID.Value,2, null,null, null);
   dm.tblStudijaLekovi.FullRefresh;
  end
else ShowMessage('�� � ��������� ��������� �� ����������. �������� � �������� !!!');
end;

procedure TfrmStudija.cbPartPropertiesChange(Sender: TObject);
begin
  if cbPart.Text <> '' then
    begin
     dm.tblIzvestajVremenskiTocki.ParamByName('id').Value:=cbPart.EditValue;
     dm.tblIzvestajVremenskiTocki.FullRefresh;
    end;
end;

//	����� �� ���������� �� �������
procedure TfrmStudija.aOdkaziBDExecute(Sender: TObject);
begin
   PanelPecatiBarkodDobrovolci.Visible:=False;
end;

procedure TfrmStudija.aOsveziListaPrimerociExecute(Sender: TObject);
begin
dm.tblPrimeroci.FullRefresh;
dm.tblChekIn.FullRefresh;
end;

procedure TfrmStudija.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      if PanelPecatiLista.Visible = true then
        begin
         PanelPecatiLista.Visible:=false;
         cbPart.Clear;
         cbRabMesto.Clear;
         cbVremenskaTocka.Clear;
        end
      else
         begin
            ModalResult := mrCancel;
            Close();
         end;
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      cxPageControl1.ActivePage:=cxTabSheet1;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmStudija.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmStudija.aPChekInListaExecute(Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

       try
        dmRes.Spremi('KS',6);
        dmKon.tblSqlReport.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        if cbPart.Text <> '' then
           dmKon.tblSqlReport.ParamByName('part_id').Value:=cbPart.EditValue
        else
           dmKon.tblSqlReport.ParamByName('part_id').Value:='%';
        if cbRabMesto.Text <> '' then
           dmKon.tblSqlReport.ParamByName('rm_id').Value:=cbRabMesto.EditValue
        else
           dmKon.tblSqlReport.ParamByName('rm_id').Value:='%';
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

//	����� �� ������� �� ������
procedure TfrmStudija.aPecatenjeListaExecute(Sender: TObject);
var  i:Integer;
begin
  if tag_izvestaj = - 1 then
    begin
        if cbVremenskaTocka.Text <> '' then
           begin
              try
                for I :=2 to 5 do
                  begin
                    dmRes.Spremi('KS',i);
                    dmKon.tblSqlReport.ParamByName('part_tocka_id').Value:=cbVremenskaTocka.EditValue;
                    dmKon.tblSqlReport.Open;
                    dmRes.frxReport1.PrintOptions.ShowDialog:=False;
                    dmRes.frxReport1.Print;
                  end;
                PanelPecatiLista.Visible:=False;
              except
              ShowMessage('�� ���� �� �� ������� ��������� !');
              end
            end
        else  ShowMessage('�������� ��������� ����� !');
    end
  else if (tag_izvestaj = 1) then
    begin
      try
        dmRes.Spremi('KS',tag_izvestaj);
        dmKon.tblSqlReport.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        if cbPart.Text <> '' then
           dmKon.tblSqlReport.ParamByName('part_naziv').Value:=cbPart.Text
        else
           dmKon.tblSqlReport.ParamByName('part_naziv').Value:='%';
        if cbRabMesto.Text <> '' then
           dmKon.tblSqlReport.ParamByName('rm_id').Value:=cbRabMesto.EditValue
        else
           dmKon.tblSqlReport.ParamByName('rm_id').Value:='%';
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
        PanelPecatiLista.Visible:=False;
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
    end
  else if (tag_izvestaj = 9) then
    begin
      try
        dmRes.Spremi('KS',tag_izvestaj);
        dmKon.tblSqlReport.ParamByName('ks_id').Value:=dm.tblStudijaID.Value;
        if cbPart.Text <> '' then
           dmKon.tblSqlReport.ParamByName('part_id').Value:=cbPart.EditValue
        else
           dmKon.tblSqlReport.ParamByName('part_id').Value:='%';
        if cbRabMesto.Text <> '' then
           dmKon.tblSqlReport.ParamByName('rm_id').Value:=cbRabMesto.EditValue
        else
           dmKon.tblSqlReport.ParamByName('rm_id').Value:='%';
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
        PanelPecatiLista.Visible:=False;
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
    end  
  else
    begin
      dm.tblIzvestajVremenskiTocki.First;
      while (not dm.tblIzvestajVremenskiTocki.Eof) do
        begin
           try
            dmRes.Spremi('KS',tag_izvestaj);
            dmKon.tblSqlReport.ParamByName('part_tocka_id').Value:=dm.tblIzvestajVremenskiTockiID.Value;
            dmKon.tblSqlReport.Open;
            dmRes.frxReport1.ShowReport();
            PanelPecatiLista.Visible:=False;
           except
            ShowMessage('�� ���� �� �� ������� ��������� !');
           end;
          dm.tblIzvestajVremenskiTocki.Next;
        end;
    end;
end;

procedure TfrmStudija.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmStudija.aPecatiTabelaPrimerociExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '���������';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmStudija.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmStudija.aPromeniStatusStudijaExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
 begin
  dm.tblStudija.Edit;
  if dm.tblStudijaSTATUS.Value = 1 then
     dm.tblStudijaSTATUS.Value:= 0
   else if dm.tblStudijaSTATUS.Value = 0 then
     dm.tblStudijaSTATUS.Value:= 1;
  dm.tblStudija.Post;
 end
else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');

end;

procedure TfrmStudija.aPVremeRealizacijaExecute(Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=9;

      PanelPecatiLista.Visible:= True;
end;

procedure TfrmStudija.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmStudija.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmStudija.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmStudija.abrisiPrimerociExecute(Sender: TObject);
begin
//
end;

procedure TfrmStudija.aDChekInListaExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',6);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDFormularBrzoSmrznuvanjeExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',3);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDFormularPodgotovkaEpruvetaPuferExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',2);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDFormularVremenskaListaCentrifugiranjeExecute(
  Sender: TObject);
begin
     dmRes.Spremi('KS',4);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDFormularVremenskaListaSkladiranjeExecute(
  Sender: TObject);
begin
     dmRes.Spremi('KS',5);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDizajnReportExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500 );
end;

procedure TfrmStudija.aDListaPrimerociExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',1);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmStudija.aDVremeRealizacijaExecute(Sender: TObject);
begin
     dmRes.Spremi('KS',9);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmStudija.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmStudija.aFormularBrzoSmrznuvanjeExecute(Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=3;

      PanelPecatiLista.Visible:= True;
end;

procedure TfrmStudija.aFormularPodgotovkaEpruvetaPuferExecute(Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=2;

      PanelPecatiLista.Visible:= True;
end;

procedure TfrmStudija.aFormularVremenskaListaCentrifugiranjeExecute(
  Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=4;

      PanelPecatiLista.Visible:= True;
end;

procedure TfrmStudija.aFormularVremenskaListaSkladiranjeExecute(
  Sender: TObject);
begin
      dm.tblIzvestajPart.close;
      dm.tblIzvestajPart.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajPart.Open;

      dm.tblIzvestajRabMesto.close;
      dm.tblIzvestajRabMesto.ParamByName('ks_studija').Value:=dm.tblStudijaID.Value;
      dm.tblIzvestajRabMesto.Open;

      tag_izvestaj:=5;

      PanelPecatiLista.Visible:= True;
end;

procedure TfrmStudija.aGenerirajPartTockiExecute(Sender: TObject);
begin
if status = 1 then
 begin
  if dm.tblStudijaPartTIP.Value = 1 then
   begin
      dm.qCountVremeRealizacija.Close;
      dm.qCountVremeRealizacija.ParamByName('studija_id').Value:=dm.tblStudijaID.Value;
      dm.qCountVremeRealizacija.ExecQuery;
      if dm.qCountVremeRealizacija.FldByName['br'].Value = 0 then
      begin
     //if (not dm.tblStudijaPartPOCETNO_VREME.IsNull) then
        // begin
         //   if(not dm.tblStudijaPartPOCETEN_DATUM.IsNull)  then
          //    begin
                if ((not dm.tblStudijaPartBROJ_TOCKI.IsNull)and (dm.tblStudijaPartBROJ_TOCKI.Value>=1))  then
                    begin
                      dm.insert6(dm.PROC_KS_PART_TOCKI, 'PART_ID', 'KS_ID', null, null, null, null,dm.tblStudijaPartID.Value,dm.tblStudijaPartSTUDIJA.Value, null,null, null, null);
                      dm.tblPartTocki.FullRefresh;
                    end
                else ShowMessage('��������� ��� �� ��������� ����� !!!')
           //   end
        //     else ShowMessage('��������� ������� ����� !!!');
      //   end
     //else ShowMessage('��������� ������� ����� !!!');
      end
     else ShowMessage('�� � ��������� ��������� �� ��������. ��������� � ���������� !!!')
   end
  else ShowMessage('����������� ���� !!!');
 end
else ShowMessage('�� � ��������� ���������� �� ��������� �����. �������� � �������� !!!');
end;

procedure TfrmStudija.aGenerirajPrimerociExecute(Sender: TObject);
var tag:Integer;
begin
   if status = 1 then
      begin
        dm.qCountVremeRealizacija.Close;
        dm.qCountVremeRealizacija.ParamByName('studija_id').Value:=dm.tblStudijaID.Value;
        dm.qCountVremeRealizacija.ExecQuery;
        if dm.qCountVremeRealizacija.FldByName['br'].Value = 0 then
           begin
             tag:=dm.return3(dm.PROC_KS_PROVERI_DEF_PARTOVI,'KS_ID',null, null,dm.tblStudijaID.Value,null,null,'TAG');
             if tag = 1 then
              begin
                dm.insert6(dm.PROC_KS_GENERIRAJ_PRIMEROCI, 'KS_ID',null, null, null, null, null,dm.tblStudijaID.Value, null, null,null, null, null);
                dm.tblPrimeroci.FullRefresh;
                dm.tblChekIn.FullRefresh;
                ShowMessage('���������� � ���� ����� �� ��������� � chek in �����!!!');
                cxPageControl2.ActivePage:=cxTabSheet7;
              end
              else ShowMessage('���������� �� ���� ������� !!!');
            end
        else ShowMessage('�� � ��������� ���������� �� ���� ����� �� ���������. ��������� � ���������� !!!')
      end
   else ShowMessage('�� � ��������� ���������� �� ���������. �������� � �������� !!!');
end;

procedure TfrmStudija.GenerirajRabMestaUcenici(state: Integer);
begin
      dm.insert6(dm.PROC_KS_RABMESTA_VRAB_UCES, 'KS_ID', 'STATE',null,null,null,null,dm.tblStudijaID.Value,state,null,null,null,null);
      dm.tblRabMestoVrabotenUcesnici.FullRefresh;
      dm.tblPrimeroci.FullRefresh;
      dm.tblChekIn.FullRefresh;
end;

procedure TfrmStudija.GenerirajPartovi(state: Integer);
begin
      dm.insert6(dm.PROC_KS_STUDIJA_PART, 'KS_ID', 'PART_BR', 'STATE', null, null, null,dm.tblStudijaID.Value, dm.tblStudijaPARTOVI.Value, state,null, null, null);
      dm.tblStudijaPart.FullRefresh;
      dm.tblPrimeroci.FullRefresh;
      dm.tblChekIn.FullRefresh;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmStudija.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmStudija.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
