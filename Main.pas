unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinOffice2013White, System.Actions, cxCheckBox, Data.DB, FIBDataSet,
  pFIBDataSet;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    aLogout: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aRabotnoMesto: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    aNaracateli: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aStudija: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    aVraboteni: TAction;
    qSetup: TpFIBDataSet;
    qSetupPARAMETAR: TFIBStringField;
    qSetupPARAM_VREDNOST: TFIBStringField;
    qSetupVREDNOST: TFIBIntegerField;
    qSetupP1: TFIBStringField;
    qSetupP2: TFIBStringField;
    qSetupV1: TFIBStringField;
    qSetupV2: TFIBStringField;
    dxBarLargeButton12: TdxBarLargeButton;
    aLogs: TAction;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aLogoutExecute(Sender: TObject);
    procedure aRabotnoMestoExecute(Sender: TObject);
    procedure aNaracateliExecute(Sender: TObject);
    procedure aStudijaExecute(Sender: TObject);
    procedure aVraboteniExecute(Sender: TObject);
    procedure aLogsExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
   
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, RabotnoMesto,
  Naracateli, Studija, MK, Vraboteni, dmUnit, Logs;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aLogoutExecute(Sender: TObject);
begin
  if (dmKon.Logout = false) then Close
  else
  begin
    SpremiForma(self);

    if dmKon.user = 'SYSDBA' then
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : �������������'
    else
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : ' + dmKon.imeprezime; // ������� �� ���������� ��������

    // Ovde otvori posebni datasetovi koi ne se otvaraat vo OtvoriTabeli()
    OtvoriTabeli();
  end;
end;

procedure TfrmMain.aLogsExecute(Sender: TObject);
begin
     frmLogs:=TfrmLogs.Create(Self, True);
     frmLogs.ShowModal;
     frmLogs.Free;
end;

procedure TfrmMain.aNaracateliExecute(Sender: TObject);
begin
     frmNaracateli:=TfrmNaracateli.Create(Self, True);
     frmNaracateli.ShowModal;
     frmNaracateli.Free;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aRabotnoMestoExecute(Sender: TObject);
begin
  frmRabotnoMesto:=TfrmRabotnoMesto.Create(Self, True);
  frmRabotnoMesto.ShowModal;
  frmRabotnoMesto.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aStudijaExecute(Sender: TObject);
begin
     frmStudija:=TfrmStudija.Create(Self, False);
     frmStudija.ShowModal;
     frmStudija.Free;
end;

procedure TfrmMain.aVraboteniExecute(Sender: TObject);
begin
  frmVraboteni:=TfrmVraboteni.Create(Self, True);
  frmVraboteni.ShowModal;
  frmVraboteni.Free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex ����� ��������
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

  frmMK:=TfrmMK.Create(nil);
  frmMK.ShowModal;
  frmMK.free;
  qSetup.Open;
      if qSetup.Locate('P2','BARCODE_PRINT',[]) then      //parametri za pecatenje barkod labeli
      begin
        dm.barcodePrinter:=qSetupV1.AsString  ;     //printer za pecatewe nalabelite
        dm.brLabeliStudija:=qSetupV2.AsInteger;     //default vrednost za broj na labali za pacienti od studija
        dm.brlLabeliOpsto:=qSetupVREDNOST.AsInteger ; // default vrednost za broj na labeli za normalni pacienti
        dm.barcodePrint:=qSetupPARAMETAR.AsString ;   //vrednost TRUE ako treba da se pa;atat labeli preku softverot

      end
      else
      begin
        dm.brLabeliStudija:=6;
        dm.brlLabeliOpsto:=3;
        dm.barcodePrint:='FALSE' ;
      end;
end;

procedure TfrmMain.OtvoriTabeli;
begin
  // ������ �� ���������� dataset-���
    dm.tblNaracateli.Open;
    dm.tblStudija.Open;
    dm.tblVraboteni.Open;
    dm.tblRabMestoVrabotenUcesnici.Open;
    dm.tblLekovi.Open;
    dm.tblStudijaLekovi.Open;
    dm.tblStudijaPart.Open;
    dm.tblPartTocki.Open;
    dm.tblPrimeroci.Open;
    dm.tblChekIn.Open;
    dm.tblIzvestajVremenskiTocki.Open;
    dm.tblLogs.Open;
end;

//TODO


end.
