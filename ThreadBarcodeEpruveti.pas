unit ThreadBarcodeEpruveti;

interface

uses
   StrUtils,Dialogs,Windows,WinINet, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
   Buttons, ExtCtrls,DateUtils,ComCtrls, pFIBDataSet,FIBDataSet,pFibScripter,FIBDatabase,
   pFIBDatabase,ActiveX,frxClass,  frxDBSet, frxDesgn, frxBarcode;

type
  TBarcodeThread = class(TThread)
  private
  protected
    procedure Execute; override;
  public
     start,stop,printer:string;ks_id, broj, tag_param:integer;
  end;
  Procedure PrintBarcodeEpruveti(printerb:string; br, ks_id,tag_param:integer);
implementation



uses dmResources, dmUnit;


Procedure PrintBarcodeEpruveti(printerb:string; br, ks_id, tag_param:integer);

var BarcodeThread: TBarcodeThread;

begin
  BarcodeThread := TBarcodeThread.Create(true);
  BarcodeThread.FreeOnTerminate := true;
  BarcodeThread.broj := br;
  BarcodeThread.printer := printerb;
  BarcodeThread.ks_id := ks_id;
  BarcodeThread.tag_param:=tag_param;
  BarcodeThread.Execute;
end;

procedure TBarcodeThread.Execute;
  var  r, j:integer;
    kod:string;
    BarCode: TfrxBarCodeObject;
begin

     CoInitialize(nil);
 if tag_param=1 then
   begin
     dm.tblBarkodEpruveti.First;
     r:=0;
     while (not dm.tblBarkodEpruveti.Eof) do
       begin
        r:=r+1;
        if (not dm.tblBarkodEpruveti.FieldByName('BARCODE').IsNull) then
           begin
               dmRes.Spremi('KS',8);
               dmRes.frxReport1.Variables.AddVariable('VAR', 'barcode', QuotedStr(dm.tblBarkodEpruvetiBARCODE.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'studija', QuotedStr(dm.tblBarkodEpruvetiKOD_STUDIJA.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'dobrovolec', QuotedStr(dm.tblBarkodEpruvetiDOBROVOLEC_ID.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'part', QuotedStr(dm.tblBarkodEpruvetiPART.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'broj_epruveta', QuotedStr(dm.tblBarkodEpruvetiBROJ_EPRUVETA_ID.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'tocka_br', QuotedStr(FloatToStr(dm.tblBarkodEpruvetiTOCKA_BR.Value)));
               dmRes.frxreport1.Variables['no']:=r;

               dmRes.frxReport1.PrintOptions.ShowDialog:=false;
               dmRes.frxReport1.PrintOptions.Printer:=Printer;
               dmRes.frxreport1.PrepareReport(true);
               //dmRes.frxreport1.ShowReport();
               dmRes.frxreport1.Print;
           end;
        dm.tblBarkodEpruveti.Next;
       end;
   end
 else if tag_param=2 then
   begin
     dm.tblBarkodEpruvetiPecati.First;
     r:=0;
     while (not dm.tblBarkodEpruvetiPecati.Eof) do
       begin
        r:=r+1;
        if (not dm.tblBarkodEpruvetiPecati.FieldByName('BARCODE').IsNull) then
           begin
               dmRes.Spremi('KS',8);
               dmRes.frxReport1.Variables.AddVariable('VAR', 'barcode', QuotedStr(dm.tblBarkodEpruvetiPecatiBARCODE.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'studija', QuotedStr(dm.tblBarkodEpruvetiPecatiKOD_STUDIJA.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'dobrovolec', QuotedStr(dm.tblBarkodEpruvetiPecatiDOBROVOLEC_ID.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'part', QuotedStr(dm.tblBarkodEpruvetiPecatiPART.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'broj_epruveta', QuotedStr(dm.tblBarkodEpruvetiPecatiBROJ_EPRUVETA_ID.Value));
               dmRes.frxreport1.Variables.AddVariable('VAR', 'tocka_br', QuotedStr(FloatToStr(dm.tblBarkodEpruvetiPecatiTOCKA_BR.Value)));
               dmRes.frxreport1.Variables['no']:=r;

               dmRes.frxReport1.PrintOptions.ShowDialog:=false;
               dmRes.frxReport1.PrintOptions.Printer:=Printer;
               dmRes.frxreport1.PrepareReport(true);
              // dmRes.frxreport1.ShowReport();
               dmRes.frxreport1.Print;
           end;
        dm.tblBarkodEpruvetiPecati.Next;
       end;
   end ;



    CoUninitialize();
end;
{procedure TBarcodeThread.Execute;
  var  i, j:integer;
    kod:string;
begin
     CoInitialize(nil);
    // frxReport1:=TfrxReport.create(nil);
     i:=strtoint(start);
      while  i<=strtoint(stop)  do
      begin
           kod:=inttostr(i);
            for j := 0 to broj-1 do
            begin
               dmRes.Spremi('KS',9);
               dmRes.frxreport1.Variables['barcode']:=kod;
               dmRes.frxreport1.Variables['no']:=j;
               dmRes.frxReport1.PrintOptions.ShowDialog:=false;
               dmRes.frxReport1.PrintOptions.Printer:=Printer;
               dmRes.frxreport1.PrepareReport(true);
               dmRes.frxreport1.Print;
            end;
            inc(i);
      end;

    CoUninitialize();
end;     }

end.
